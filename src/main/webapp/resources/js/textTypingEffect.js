let i = 0;
let d;
let cursor = 0;
const words = ['Project', 'Science', 'Innovation', 'Software']
let text;

function textTypingEffect() {
	text = words[cursor];
	if (i < text.length) {
		document.body.querySelector("#app-title").innerHTML += text.charAt(i);
		i++;
		setTimeout(textTypingEffect, 300);
		if (i == text.length - 1) {
			if (cursor < words.length - 1) {
				d = text.length - 1;
			} else {
				d = -2;
			}
		}
	} else if (d >= -1) {
		document.body.querySelector("#app-title").innerHTML = document.body.querySelector("#app-title").innerHTML.slice(0, -1);
		d--;
		setTimeout(textTypingEffect, 100);
		if (d == -1) {
			i = 0;
			cursor++;
		}
	}
}

textTypingEffect();