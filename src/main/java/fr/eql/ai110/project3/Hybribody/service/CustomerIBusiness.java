package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Customer;

public interface CustomerIBusiness {

	List<Customer> getAll();
	Customer getById(Integer id);
	List<Customer> getAllWithoutOneCustomer(Integer idCustomerNoWanted);
}
