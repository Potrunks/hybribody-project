package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "positivetarget")
public class PositiveTarget implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idpositivetarget")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "idconsumable")
	private Target target;
	@OneToMany(mappedBy = "positiveTarget", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ScreeningRound> rounds;
	@ManyToOne
	@JoinColumn(name = "idvalue")
	private ScientificValue finalConcentration;
	
	public PositiveTarget() {
		super();
	}

	public PositiveTarget(Integer id, Target target, List<ScreeningRound> rounds, ScientificValue finalConcentration) {
		super();
		this.id = id;
		this.target = target;
		this.rounds = rounds;
		this.finalConcentration = finalConcentration;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public List<ScreeningRound> getRounds() {
		return rounds;
	}

	public void setRounds(List<ScreeningRound> rounds) {
		this.rounds = rounds;
	}

	public ScientificValue getFinalConcentration() {
		return finalConcentration;
	}

	public void setFinalConcentration(ScientificValue finalConcentration) {
		this.finalConcentration = finalConcentration;
	}
	
}
