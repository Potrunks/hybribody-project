package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Antibody;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;

public interface AntibodyIDAO extends CrudRepository<Antibody, Long> {

	List<Antibody> findByMotherPlatePicking(MotherPlatePicking motherPlatePicking);
}
