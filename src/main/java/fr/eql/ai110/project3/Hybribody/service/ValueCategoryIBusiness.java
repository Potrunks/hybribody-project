package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.ValueCategory;

public interface ValueCategoryIBusiness {

	List<ValueCategory> getAllSubCategoryByExperimentName(String category, String experiment);
}
