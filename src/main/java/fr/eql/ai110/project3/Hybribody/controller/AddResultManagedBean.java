package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.model.ValueCategory;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;
import fr.eql.ai110.project3.Hybribody.service.MotherPlatePickingIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ScreeningIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ScreeningRoundIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ValueCategoryIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ValueIBusiness;

@Controller(value = "mbAddResult")
@Scope(value = "session")
public class AddResultManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Employee employee;
	private List<Project> projects = new ArrayList<Project>();
	private Integer selectedProject = null;
	private List<MotherPlatePicking> motherPlates = new ArrayList<MotherPlatePicking>();
	private Integer selectedMotherPlate = null;
	private boolean selectedSuccess = true;
	private String inputFailure = "";
	private MotherPlatePicking motherPlatePicking = new MotherPlatePicking();
	private Experiment xp = new Experiment();
	private Project project = new Project();
	private List<Screening> screenings = new ArrayList<Screening>();
	private Integer selectedScreening = null;
	private List<ScreeningRound> rounds = new ArrayList<ScreeningRound>();
	private Integer selectedRound = null;
	private List<ValueCategory> inputDilutions = new ArrayList<ValueCategory>();
	private List<ValueCategory> outputDilutions = new ArrayList<ValueCategory>();
	private Float selectedInputDilution1 = 0f;
	private Integer inputClone1 = 0;
	private Float inputResult1 = 0f;
	private Float selectedInputDilution2 = 0f;
	private Integer inputClone2 = 0;
	private Float inputResult2 = 0f;
	private Float selectedOutputDilution1 = 0f;
	private Integer outputClone1 = 0;
	private Float outputResult1 = 0f;
	private Float selectedOutputDilution2 = 0f;
	private Integer outputClone2 = 0;
	private Float outputResult2 = 0f;
	private Float selectedOutputDilution3 = 0f;
	private Integer outputClone3 = 0;
	private Float outputResult3 = 0f;
	private Float meanInput = 0f;
	private Float meanOutput = 0f;
	private Float ratio = 0f;
	private Float enrichment = 0f;
	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private MotherPlatePickingIBusiness motherPlateBusiness;
	@Autowired
	private ScreeningIBusiness screeningBusiness;
	@Autowired
	private ScreeningRoundIBusiness roundBusiness;
	@Autowired
	private ValueCategoryIBusiness valueCategoryBusiness;
	@Autowired
	private ValueIBusiness valueBusiness;
	@Autowired
	private ExperimentIBusiness experimentBusiness;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}

	// region Generique Experiment Part

	public String loadAppropriateResultFormFromProject(Integer idProject) {
		String redirect = null;
		xp = experimentBusiness.getTheMostRecentXPInProgress(idProject);
		String categoryXP = experimentBusiness.getCategory(xp.getId());
		if (categoryXP.equals("mother_plate_screening")) {
			redirect = loadProjectsInProgressWithMotherPlateInProgressAssociatedShortcut(xp.getId());
		} else if (categoryXP.equals("screening")) {
			redirect = loadProjectsInProgressWithScreeningAndRoundInProgressAssociated();
		}
		return redirect;
	}

	// end region

	// region Screening Round Result Part

	public String loadProjectsInProgressWithScreeningAndRoundInProgressAssociated() {
		String redirect = null;
		reset();
		projects = projectBusiness.getAllInProgressWithRoundInProgress();
		inputDilutions = valueCategoryBusiness.getAllSubCategoryByExperimentName("Dilution Input", "Screening Round");
		outputDilutions = valueCategoryBusiness.getAllSubCategoryByExperimentName("Dilution Output", "Screening Round");
		if (projects != null) {
			redirect = "/addScreeningRoundResult.xhtml?faces-redirect=true";
		} else {
			redirect = "/addResult.xhtml?faces-redirect=false";
		}
		return redirect;
	}

	public void onProjectChangeForScreening() {
		if (selectedProject != null) {
			screenings = screeningBusiness.getAllInProgressWithRoundInProgressByProject(selectedProject);
			selectedScreening = null;
		} else {
			screenings = new ArrayList<Screening>();
			selectedScreening = null;
		}
	}

	public void onScreeningChangeForRound() {
		if (selectedScreening != null) {
			rounds = roundBusiness.getRoundsInProgress(screeningBusiness.getById(selectedScreening));
		} else {
			rounds = new ArrayList<ScreeningRound>();
			selectedRound = null;
		}
	}

	public void calculateRoundResults() {
		inputResult1 = valueBusiness.calculateInputFromNbCloneAndDilution(inputClone1, selectedInputDilution1);
		inputResult2 = valueBusiness.calculateInputFromNbCloneAndDilution(inputClone2, selectedInputDilution2);
		outputResult1 = valueBusiness.calculateOutputFromNbCloneAndDilution(outputClone1, selectedOutputDilution1);
		outputResult2 = valueBusiness.calculateOutputFromNbCloneAndDilution(outputClone2, selectedOutputDilution2);
		outputResult3 = valueBusiness.calculateOutputFromNbCloneAndDilution(outputClone3, selectedOutputDilution3);
		meanInput = valueBusiness.calculateMeanInputFromInputResults(inputResult1, inputResult2);
		meanOutput = valueBusiness.calculateMeanOutputFromOutputResults(outputResult1, outputResult2, outputResult3);
		ratio = valueBusiness.calculateRatioFromMeanResults(meanInput, meanOutput);
		if (verifyPreviousRoundExist() == true && ratio != 0f) {
			enrichment = valueBusiness.calculateEnrichmentBetweenRounds(ratio,
					valueBusiness.getRatioValueFromPreviousRound(selectedScreening, selectedRound));
		}
	}

	public boolean verifyPreviousRoundExist() {
		boolean exist = false;
		if (selectedScreening != null && selectedRound != null) {
			exist = roundBusiness.verifyByScreeningIfPreviousRoundExist(selectedScreening, selectedRound);
		}
		return exist;
	}

	public String addScreeningRoundResult() {
		String redirect = null;
		if (selectedProject == null || selectedScreening == null || selectedRound == null) {
			if (selectedProject == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Project selected");
				fm.setDetail("No Project selected");
				FacesContext.getCurrentInstance().addMessage("addResultForm:projectList", fm);
			}
			if (selectedScreening == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Screening selected");
				fm.setDetail("No Screening selected");
				FacesContext.getCurrentInstance().addMessage("addResultForm:screeningList", fm);
			}
			if (selectedRound == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Screening Round selected");
				fm.setDetail("No Screening Round selected");
				FacesContext.getCurrentInstance().addMessage("addResultForm:roundList", fm);
			}
			redirect = "/addScreeningRoundResult.xhtml?faces-redirect=false";
		} else {
			if (verifyAuthorizationForScreeningRound() == false) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("Not the creator of the experiment or Manager of the laboratory");
				fm.setDetail("Not the creator of the experiment or Manager of the laboratory");
				FacesContext.getCurrentInstance().addMessage("addResultForm:authorization", fm);
				redirect = "/addScreeningRoundResult.xhtml?faces-redirect=false";
			} else {
				roundBusiness.validate(selectedRound, inputFailure, selectedSuccess);
				valueBusiness.validateInputResultForScreeningRound(selectedRound, inputResult1, inputResult2,
						selectedInputDilution1, selectedInputDilution2);
				valueBusiness.validateOutputResultForScreeningRound(selectedRound, outputResult1, outputResult2,
						outputResult3, selectedOutputDilution1, selectedOutputDilution2, selectedOutputDilution3);
				valueBusiness.validateMeanInputResultForScreeningRound(selectedRound, meanInput);
				valueBusiness.validateMeanOutputResultForScreeningRound(selectedRound, meanOutput);
				valueBusiness.validateRatioResultForScreeningRound(selectedRound, ratio);
				valueBusiness.validateEnrichmentResultForScreeningRound(selectedRound, enrichment);
				xp = screeningBusiness.getById(selectedScreening);
				redirect = "/addResultSuccess.xhtml?faces-redirect=true";
			}
		}
		return redirect;
	}

	public boolean verifyRoundInProgressExist() {
		boolean exist = roundBusiness.verifyRoundInProgressExist();
		return exist;
	}

	// endregion

	// region Mother Plate Result Part

	public String loadProjectsInProgressWithMotherPlateInProgressAssociated() {
		String redirect = null;
		reset();
		projects = projectBusiness.getAllInProgressAndMotherPlateInProgress();
		if (!projects.isEmpty()) {
			redirect = "/addResultMotherPlate.xhtml?faces-redirect=true";
		} else {
			redirect = "/addResult.xhtml?faces-redirect=false";
		}
		return redirect;
	}

	public String loadProjectsInProgressWithMotherPlateInProgressAssociatedShortcut(Integer idMotherPlatePicking) {
		String redirect = null;
		reset();
		motherPlatePicking = motherPlateBusiness.getById(idMotherPlatePicking);
		project = projectBusiness.getByExperiment(motherPlatePicking);
		selectedMotherPlate = motherPlatePicking.getId();
		selectedProject = project.getIdproject();
		redirect = "/addResultMotherPlateShortcut.xhtml?faces-redirect=true";
		return redirect;
	}

	public void onProjectChangeForMotherPlate() {
		if (selectedProject != null) {
			motherPlates = motherPlateBusiness.getAllInProgressByIdProject(selectedProject);
		} else {
			motherPlates = new ArrayList<MotherPlatePicking>();
		}
	}

	public String addMotherPlateResult() {
		String redirect = null;
		if (selectedProject == null || selectedMotherPlate == null) {
			if (selectedProject == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Project selected");
				fm.setDetail("No Project selected");
				FacesContext.getCurrentInstance().addMessage("addResultForm:projectList", fm);
			}
			if (selectedMotherPlate == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Mother Plate selected");
				fm.setDetail("No Mother Plate selected");
				FacesContext.getCurrentInstance().addMessage("addResultForm:motherPlateList", fm);
			}
			redirect = "/addResultMotherPlate.xhtml?faces-redirect=false";
		} else {
			if (verifyAuthorizationForMotherPlatePicking() == false) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("Not the creator of the experiment or Manager of the laboratory");
				fm.setDetail("Not the creator of the experiment or Manager of the laboratory");
				FacesContext.getCurrentInstance().addMessage("addResultForm:authorization", fm);
				redirect = "/addResultMotherPlate.xhtml?faces-redirect=false";
			} else {
				motherPlateBusiness.validate(selectedMotherPlate, selectedSuccess, inputFailure);
				xp = motherPlateBusiness.getById(selectedMotherPlate);
				redirect = "/addResultSuccess.xhtml?faces-redirect=true";
			}
		}
		return redirect;
	}

	public boolean verifyMotherPlateInProgress() {
		boolean isPresent = motherPlateBusiness.verifyMotherPlateInProgressExist();
		return isPresent;
	}

	// endregion

	// region Other

	private boolean verifyAuthorizationForMotherPlatePicking() {
		boolean isAuthorize = true;
		if (experimentBusiness.getById(selectedMotherPlate).getEmployee().getId() != employee.getId()
				&& !employee.getTitle().getName().equals("Manager")) {
			isAuthorize = false;
		}
		return isAuthorize;
	}
	
	private boolean verifyAuthorizationForScreeningRound() {
		boolean isAuthorize = true;
		if (experimentBusiness.getById(selectedScreening).getEmployee().getId() != employee.getId()
				&& !employee.getTitle().getName().equals("Manager")) {
			isAuthorize = false;
		}
		return isAuthorize;
	}

	private void reset() {
		projects = new ArrayList<Project>();
		selectedProject = null;
		motherPlates = new ArrayList<MotherPlatePicking>();
		selectedMotherPlate = null;
		selectedSuccess = true;
		inputFailure = "";
		motherPlatePicking = new MotherPlatePicking();
		xp = new Experiment();
		project = new Project();
		screenings = new ArrayList<Screening>();
		selectedScreening = null;
		rounds = new ArrayList<ScreeningRound>();
		selectedRound = null;
		inputDilutions = new ArrayList<ValueCategory>();
		outputDilutions = new ArrayList<ValueCategory>();
		selectedInputDilution1 = 0f;
		inputClone1 = 0;
		inputResult1 = 0f;
		selectedInputDilution2 = 0f;
		inputClone2 = 0;
		inputResult2 = 0f;
		selectedOutputDilution1 = 0f;
		outputClone1 = 0;
		outputResult1 = 0f;
		selectedOutputDilution2 = 0f;
		outputClone2 = 0;
		outputResult2 = 0f;
		selectedOutputDilution3 = 0f;
		outputClone3 = 0;
		outputResult3 = 0f;
		meanInput = 0f;
		meanOutput = 0f;
		ratio = 0f;
		enrichment = 0f;
	}

	// endregion

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Integer getSelectedProject() {
		return selectedProject;
	}

	public void setSelectedProject(Integer selectedProject) {
		this.selectedProject = selectedProject;
	}

	public List<MotherPlatePicking> getMotherPlates() {
		return motherPlates;
	}

	public void setMotherPlates(List<MotherPlatePicking> motherPlates) {
		this.motherPlates = motherPlates;
	}

	public boolean isSelectedSuccess() {
		return selectedSuccess;
	}

	public void setSelectedSuccess(boolean selectedSuccess) {
		this.selectedSuccess = selectedSuccess;
	}

	public String getInputFailure() {
		return inputFailure;
	}

	public void setInputFailure(String inputFailure) {
		this.inputFailure = inputFailure;
	}

	public Integer getSelectedMotherPlate() {
		return selectedMotherPlate;
	}

	public void setSelectedMotherPlate(Integer selectedMotherPlate) {
		this.selectedMotherPlate = selectedMotherPlate;
	}

	public MotherPlatePicking getMotherPlatePicking() {
		return motherPlatePicking;
	}

	public void setMotherPlatePicking(MotherPlatePicking motherPlatePicking) {
		this.motherPlatePicking = motherPlatePicking;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Experiment getXp() {
		return xp;
	}

	public void setXp(Experiment xp) {
		this.xp = xp;
	}

	public List<Screening> getScreenings() {
		return screenings;
	}

	public void setScreenings(List<Screening> screenings) {
		this.screenings = screenings;
	}

	public Integer getSelectedScreening() {
		return selectedScreening;
	}

	public void setSelectedScreening(Integer selectedScreening) {
		this.selectedScreening = selectedScreening;
	}

	public List<ScreeningRound> getRounds() {
		return rounds;
	}

	public void setRounds(List<ScreeningRound> rounds) {
		this.rounds = rounds;
	}

	public Integer getSelectedRound() {
		return selectedRound;
	}

	public void setSelectedRound(Integer selectedRound) {
		this.selectedRound = selectedRound;
	}

	public List<ValueCategory> getInputDilutions() {
		return inputDilutions;
	}

	public void setInputDilutions(List<ValueCategory> inputDilutions) {
		this.inputDilutions = inputDilutions;
	}

	public List<ValueCategory> getOutputDilutions() {
		return outputDilutions;
	}

	public void setOutputDilutions(List<ValueCategory> outputDilutions) {
		this.outputDilutions = outputDilutions;
	}

	public Float getSelectedInputDilution1() {
		return selectedInputDilution1;
	}

	public void setSelectedInputDilution1(Float selectedInputDilution1) {
		if (selectedInputDilution1 == null) {
			selectedInputDilution1 = 0f;
		}
		this.selectedInputDilution1 = selectedInputDilution1;
	}

	public Integer getInputClone1() {
		return inputClone1;
	}

	public void setInputClone1(Integer inputClone1) {
		if (inputClone1 == null) {
			inputClone1 = 0;
		}
		this.inputClone1 = inputClone1;
	}

	public Float getInputResult1() {
		return inputResult1;
	}

	public void setInputResult1(Float inputResult1) {
		this.inputResult1 = inputResult1;
	}

	public Float getSelectedInputDilution2() {
		return selectedInputDilution2;
	}

	public void setSelectedInputDilution2(Float selectedInputDilution2) {
		if (selectedInputDilution2 == null) {
			selectedInputDilution2 = 0f;
		}
		this.selectedInputDilution2 = selectedInputDilution2;
	}

	public Integer getInputClone2() {
		return inputClone2;
	}

	public void setInputClone2(Integer inputClone2) {
		if (inputClone2 == null) {
			inputClone2 = 0;
		}
		this.inputClone2 = inputClone2;
	}

	public Float getInputResult2() {
		return inputResult2;
	}

	public void setInputResult2(Float inputResult2) {
		this.inputResult2 = inputResult2;
	}

	public Float getSelectedOutputDilution1() {
		return selectedOutputDilution1;
	}

	public void setSelectedOutputDilution1(Float selectedOutputDilution1) {
		if (selectedOutputDilution1 == null) {
			selectedOutputDilution1 = 0f;
		}
		this.selectedOutputDilution1 = selectedOutputDilution1;
	}

	public Integer getOutputClone1() {
		return outputClone1;
	}

	public void setOutputClone1(Integer outputClone1) {
		if (outputClone1 == null) {
			outputClone1 = 0;
		}
		this.outputClone1 = outputClone1;
	}

	public Float getOutputResult1() {
		return outputResult1;
	}

	public void setOutputResult1(Float outputResult1) {
		this.outputResult1 = outputResult1;
	}

	public Float getSelectedOutputDilution2() {
		return selectedOutputDilution2;
	}

	public void setSelectedOutputDilution2(Float selectedOutputDilution2) {
		if (selectedOutputDilution2 == null) {
			selectedOutputDilution2 = 0f;
		}
		this.selectedOutputDilution2 = selectedOutputDilution2;
	}

	public Integer getOutputClone2() {
		return outputClone2;
	}

	public void setOutputClone2(Integer outputClone2) {
		if (outputClone2 == null) {
			outputClone2 = 0;
		}
		this.outputClone2 = outputClone2;
	}

	public Float getOutputResult2() {
		return outputResult2;
	}

	public void setOutputResult2(Float outputResult2) {
		this.outputResult2 = outputResult2;
	}

	public Float getSelectedOutputDilution3() {
		return selectedOutputDilution3;
	}

	public void setSelectedOutputDilution3(Float selectedOutputDilution3) {
		if (selectedOutputDilution3 == null) {
			selectedOutputDilution3 = 0f;
		}
		this.selectedOutputDilution3 = selectedOutputDilution3;
	}

	public Integer getOutputClone3() {
		return outputClone3;
	}

	public void setOutputClone3(Integer outputClone3) {
		if (outputClone3 == null) {
			outputClone3 = 0;
		}
		this.outputClone3 = outputClone3;
	}

	public Float getOutputResult3() {
		return outputResult3;
	}

	public void setOutputResult3(Float outputResult3) {
		this.outputResult3 = outputResult3;
	}

	public Float getMeanInput() {
		return meanInput;
	}

	public void setMeanInput(Float meanInput) {
		if (meanInput == null) {
			meanInput = 0f;
		}
		this.meanInput = meanInput;
	}

	public Float getMeanOutput() {
		return meanOutput;
	}

	public void setMeanOutput(Float meanOutput) {
		if (meanOutput == null) {
			meanOutput = 0f;
		}
		this.meanOutput = meanOutput;
	}

	public Float getRatio() {
		return ratio;
	}

	public void setRatio(Float ratio) {
		if (ratio == null) {
			ratio = 0f;
		}
		this.ratio = ratio;
	}

	public Float getEnrichment() {
		return enrichment;
	}

	public void setEnrichment(Float enrichment) {
		if (enrichment == null) {
			enrichment = 0f;
		}
		this.enrichment = enrichment;
	}

}
