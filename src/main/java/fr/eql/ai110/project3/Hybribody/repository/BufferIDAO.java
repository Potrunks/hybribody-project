package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.Buffer;

public interface BufferIDAO extends CrudRepository<Buffer, Long> {

	@Query(value="SELECT c.* "
			+ "FROM consumable c "
			+ "where c.category = 'buffer' "
			+ "and c.application = :application "
			+ "group by c.name", nativeQuery = true)
	List<Buffer> findByApplicationIs(@Param("application") String application);
	Buffer findById(Integer idBuffer);
	@Query(value="SELECT c.* "
			+ "FROM consumable c "
			+ "inner join scientific_value v on v.idvalue = c.idvalue "
			+ "inner join valuecategory vc on vc.idvaluecategory = v.idvaluecategory "
			+ "where c.name = :name "
			+ "and v.value_result = :value_result "
			+ "and vc.name = 'pH'", nativeQuery = true)
	Buffer findByNameAndPH(@Param("name") String name,@Param("value_result") Float value);
}
