package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "scientific_value")
public class ScientificValue implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idvalue")
	private Integer id;
	@Column(name="value_result")
	private Float valueResult;
	@ManyToOne
	@JoinColumn(name="idvaluecategory")
	private ValueCategory category;
	@ManyToOne
	@JoinColumn(name="idscreeninground")
	private ScreeningRound round;
	@ManyToOne
	@JoinColumn(name="idunity")
	private Unity unity;
	@OneToMany(mappedBy = "finalConcentration", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<PositiveTarget> positiveTargets;
	@OneToMany(mappedBy = "finalConcentration", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<NegativeTarget> negativeTargets;
	@OneToMany(mappedBy = "concentration", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Molecule> molecules;
	@OneToMany(mappedBy = "temperature", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ExperimentalCondition> tempConditions;
	@OneToMany(mappedBy = "pH", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Buffer> pHBuffers;
	@OneToMany(mappedBy = "complexity", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Library> librairies;
	
	public ScientificValue() {
		super();
	}

	public ScientificValue(Integer id, Float valueResult, ValueCategory category, ScreeningRound round, Unity unity,
			List<PositiveTarget> positiveTargets, List<NegativeTarget> negativeTargets, List<Molecule> molecules,
			List<ExperimentalCondition> tempConditions, List<Buffer> pHBuffers, List<Library> librairies) {
		super();
		this.id = id;
		this.valueResult = valueResult;
		this.category = category;
		this.round = round;
		this.unity = unity;
		this.positiveTargets = positiveTargets;
		this.negativeTargets = negativeTargets;
		this.molecules = molecules;
		this.tempConditions = tempConditions;
		this.pHBuffers = pHBuffers;
		this.librairies = librairies;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ValueCategory getCategory() {
		return category;
	}

	public void setCategory(ValueCategory category) {
		this.category = category;
	}

	public ScreeningRound getRound() {
		return round;
	}

	public void setRound(ScreeningRound round) {
		this.round = round;
	}

	public Unity getUnity() {
		return unity;
	}

	public void setUnity(Unity unity) {
		this.unity = unity;
	}

	public List<PositiveTarget> getPositiveTargets() {
		return positiveTargets;
	}

	public void setPositiveTargets(List<PositiveTarget> positiveTargets) {
		this.positiveTargets = positiveTargets;
	}

	public List<NegativeTarget> getNegativeTargets() {
		return negativeTargets;
	}

	public void setNegativeTargets(List<NegativeTarget> negativeTargets) {
		this.negativeTargets = negativeTargets;
	}

	public List<Molecule> getMolecules() {
		return molecules;
	}

	public void setMolecules(List<Molecule> molecules) {
		this.molecules = molecules;
	}

	public List<Buffer> getpHBuffers() {
		return pHBuffers;
	}

	public void setpHBuffers(List<Buffer> pHBuffers) {
		this.pHBuffers = pHBuffers;
	}

	public List<ExperimentalCondition> getTempConditions() {
		return tempConditions;
	}

	public void setTempConditions(List<ExperimentalCondition> tempConditions) {
		this.tempConditions = tempConditions;
	}

	public Float getValueResult() {
		return valueResult;
	}

	public void setValueResult(Float valueResult) {
		this.valueResult = valueResult;
	}

	public List<Library> getLibrairies() {
		return librairies;
	}

	public void setLibrairies(List<Library> librairies) {
		this.librairies = librairies;
	}

}
