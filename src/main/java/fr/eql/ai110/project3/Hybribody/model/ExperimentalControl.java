package fr.eql.ai110.project3.Hybribody.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("experimental_control")
public class ExperimentalControl extends Consumable {

	private static final long serialVersionUID = 1L;

	@Column(name="position")
	private String position;
	@Column(name="target_recognize")
	private String targetRecognize;
	@ManyToMany
	@JoinTable(name = "T_motherplate_xpcontrol_Associations",
			joinColumns = @JoinColumn(name = "idconsumable"),
			inverseJoinColumns = @JoinColumn(name = "idexperiment"))
	private List<MotherPlatePicking> controlMotherPlate;
	
	public ExperimentalControl() {
		super();
	}

	public ExperimentalControl(String position, String targetRecognize, List<MotherPlatePicking> controlMotherPlate) {
		super();
		this.position = position;
		this.targetRecognize = targetRecognize;
		this.controlMotherPlate = controlMotherPlate;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTargetRecognize() {
		return targetRecognize;
	}

	public void setTargetRecognize(String targetRecognize) {
		this.targetRecognize = targetRecognize;
	}

	public List<MotherPlatePicking> getControlMotherPlate() {
		return controlMotherPlate;
	}

	public void setControlMotherPlate(List<MotherPlatePicking> controlMotherPlate) {
		this.controlMotherPlate = controlMotherPlate;
	}
	
}
