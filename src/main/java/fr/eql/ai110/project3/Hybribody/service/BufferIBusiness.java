package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Buffer;

public interface BufferIBusiness {

	/**
	 * Get all buffer available for the screening type wanted
	 * @param screeningType : type of screening
	 * @return List of buffer available for this type of screening
	 */
	List<Buffer> getAllByScreeningType(String screeningType);
	/**
	 * Get a buffer by this ID
	 * @param idBuffer : ID of the buffer wanted
	 * @return the buffer with the given ID
	 */
	Buffer getByIdBuffer(Integer idBuffer);
	/**
	 * Get a buffer by this name and this pH value
	 * @param bufferName : name of the buffer wanted
	 * @param pH : pH value wanted
	 * @return a buffer with the name and the pH value wanted
	 */
	Buffer getByNameAndPH(String bufferName, Float pH);
}
