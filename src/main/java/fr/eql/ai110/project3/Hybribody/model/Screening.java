package fr.eql.ai110.project3.Hybribody.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("screening")
public class Screening extends Experiment {

	private static final long serialVersionUID = 1L;

	@Column(name="batch")
	private Integer batch;
	@OneToMany(mappedBy = "screening", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private List<ScreeningRound> rounds;
	@ManyToOne
	@JoinColumn(name="id_library")
	private Library library;
	@ManyToOne
	@JoinColumn(name="idscreeningcategory")
	private ScreeningCategory category;
	
	public Screening() {
		super();
	}

	public Screening(Integer batch, List<ScreeningRound> rounds, Library library, ScreeningCategory category) {
		super();
		this.batch = batch;
		this.rounds = rounds;
		this.library = library;
		this.category = category;
	}

	public Integer getBatch() {
		return batch;
	}

	public void setBatch(Integer batch) {
		this.batch = batch;
	}

	public List<ScreeningRound> getRounds() {
		return rounds;
	}

	public void setRounds(List<ScreeningRound> rounds) {
		this.rounds = rounds;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

	public ScreeningCategory getCategory() {
		return category;
	}

	public void setCategory(ScreeningCategory category) {
		this.category = category;
	}

}
