package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "antibody")
public class Antibody implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_antibody")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="position")
	private String position;
	@Column(name="date_creation")
	private LocalDate dateCreation;
	@ManyToOne
	@JoinColumn(name = "idexperiment")
	private MotherPlatePicking motherPlatePicking;
	@ManyToOne
	@JoinColumn(name = "idscreeninground")
	private ScreeningRound screeningRoundOrigin;
	
	public Antibody() {
		super();
	}

	public Antibody(Integer id, String name, String position, LocalDate dateCreation,
			MotherPlatePicking motherPlatePicking, ScreeningRound screeningRoundOrigin) {
		super();
		this.id = id;
		this.name = name;
		this.position = position;
		this.dateCreation = dateCreation;
		this.motherPlatePicking = motherPlatePicking;
		this.screeningRoundOrigin = screeningRoundOrigin;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public MotherPlatePicking getMotherPlatePicking() {
		return motherPlatePicking;
	}

	public void setMotherPlatePicking(MotherPlatePicking motherPlatePicking) {
		this.motherPlatePicking = motherPlatePicking;
	}

	public ScreeningRound getScreeningRoundOrigin() {
		return screeningRoundOrigin;
	}

	public void setScreeningRoundOrigin(ScreeningRound screeningRoundOrigin) {
		this.screeningRoundOrigin = screeningRoundOrigin;
	}
	
}
