package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Customer;
import fr.eql.ai110.project3.Hybribody.repository.CustomerIDAO;
import fr.eql.ai110.project3.Hybribody.service.CustomerIBusiness;

@Service
public class CustomerBusiness implements CustomerIBusiness {
	
	@Autowired
	private CustomerIDAO customerDAO;
	
	@Override
	public List<Customer> getAll() {
		return customerDAO.findAll();
	}

	@Override
	public Customer getById(Integer id) {
		return customerDAO.findById(id);
	}

	@Override
	public List<Customer> getAllWithoutOneCustomer(Integer idCustomerNoWanted) {
		return customerDAO.findByIdNot(idCustomerNoWanted);
	}

}
