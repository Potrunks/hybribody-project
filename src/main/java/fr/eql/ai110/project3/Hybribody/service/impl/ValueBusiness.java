package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.model.Unity;
import fr.eql.ai110.project3.Hybribody.model.ScientificValue;
import fr.eql.ai110.project3.Hybribody.model.ValueCategory;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningRoundIDAO;
import fr.eql.ai110.project3.Hybribody.repository.UnityIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ValueCategoryIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ValueIDAO;
import fr.eql.ai110.project3.Hybribody.service.ValueIBusiness;

@Service
public class ValueBusiness implements ValueIBusiness {

	private Integer dilutionMultiplicator = 100;

	@Autowired
	private ValueIDAO valueDAO;
	@Autowired
	private ScreeningRoundIDAO screeningRoundDAO;
	@Autowired
	private ScreeningIDAO screeningDAO;
	@Autowired
	private ValueCategoryIDAO valueCategoryDAO;
	@Autowired
	private UnityIDAO unityDAO;

	@Override
	public ScientificValue getByScreeningRoundIdAndValueCategoryName(Integer idRound, String valueCatName) {
		return valueDAO.findByScreeningRoundIdAndValueCategoryName(idRound, valueCatName);
	}

	@Override
	public Float calculateInputFromNbCloneAndDilution(Integer nbClones, Float dilution) {
		Float result;
		result = nbClones * dilution;
		return result;
	}

	@Override
	public Float calculateOutputFromNbCloneAndDilution(Integer nbClones, Float dilution) {
		Float result;
		result = nbClones * dilution * dilutionMultiplicator;
		return result;
	}

	@Override
	public Float calculateMeanInputFromInputResults(Float inputResult1, Float inputResult2) {
		Float result = 0f;
		float[] inputResults = new float[2];
		inputResults[0] = inputResult1;
		inputResults[1] = inputResult2;
		result = meanCalcul(inputResults);
		return result;
	}

	@Override
	public Float calculateMeanOutputFromOutputResults(Float outputResult1, Float outputResult2, Float outputResult3) {
		Float result = 0f;
		float[] outputResults = new float[3];
		outputResults[0] = outputResult1;
		outputResults[1] = outputResult2;
		outputResults[2] = outputResult3;
		result = meanCalcul(outputResults);
		return result;
	}

	@Override
	public Float calculateRatioFromMeanResults(Float meanInput, Float meanOutput) {
		Float result = 0f;
		if (meanInput != 0f) {
			result = meanOutput / meanInput;
		}
		return result;
	}

	@Override
	public Float calculateEnrichmentBetweenRounds(Float newRatio, Float previousRatio) {
		Float result = 0f;
		if (previousRatio != 0f) {
			result = newRatio / previousRatio;
		}
		return result;
	}

	private Float meanCalcul(float[] results) {
		Float mean = 0f;
		Float sum = 0f;
		Integer nbOfAvailableResult = 0;
		for (int i = 0; i < results.length; i++) {
			if (results[i] != 0f) {
				sum += results[i];
				nbOfAvailableResult++;
			}
		}
		if (nbOfAvailableResult != 0) {
			mean = sum / nbOfAvailableResult;
		}
		return mean;
	}

	@Override
	public Float getRatioValueFromPreviousRound(Integer idScreening, Integer idRound) {
		Float previousRatio = 0f;
		ScreeningRound round = screeningRoundDAO.findById(idRound);
		Screening screening = screeningDAO.findById(idScreening);
		for (ScreeningRound scrRo : screening.getRounds()) {
			if (scrRo.getNb() == round.getNb() - 1 && scrRo.isValid() == true) {
				for (ScientificValue value : scrRo.getValues()) {
					if (value.getCategory().getName().equals("Ratio")) {
						previousRatio = value.getValueResult();
					}
				}
			}
		}
		return previousRatio;
	}

	@Override
	public void validateInputResultForScreeningRound(Integer idScreeningRound, Float inputResult1, Float inputResult2,
			Float dilutionValue1, Float dilutionValue2) {
		addNewValueForScreeningRound(idScreeningRound, inputResult1, dilutionValue1, "Dilution Input", "phages");
		addNewValueForScreeningRound(idScreeningRound, inputResult2, dilutionValue2, "Dilution Input", "phages");
	}

	private void addNewValueForScreeningRound(Integer idScreeningRound, Float value, Float dilutionValue,
			String categoryValue, String unityValue) {
		if (value != 0f) {
			ScientificValue newValue = new ScientificValue();
			newValue.setRound(screeningRoundDAO.findById(idScreeningRound));
			newValue.setValueResult(value);
			if (dilutionValue != null) {
				for (ValueCategory valueCategory : valueCategoryDAO.findByDilutionValue(dilutionValue)) {
					if (valueCategoryDAO.findById(valueCategory.getIdParent()).getName().equals(categoryValue)) {
						newValue.setCategory(valueCategory);
						for (Unity unity : unityDAO.findByCategories(valueCategory)) {
							if (unity.getName().equals(unityValue)) {
								newValue.setUnity(unity);
							}
						}
					}
				}
			} else {
				newValue.setCategory(valueCategoryDAO.findByName(categoryValue));
				for (Unity unity : unityDAO.findByCategories(newValue.getCategory())) {
					if (unity.getName().equals(unityValue)) {
						newValue.setUnity(unity);
					}
				}
			}
			valueDAO.save(newValue);
		}
	}

	@Override
	public void validateMeanInputResultForScreeningRound(Integer idScreeningRound, Float meanInput) {
		addNewValueForScreeningRound(idScreeningRound, meanInput, null, "Mean Input", "phages");
	}

	@Override
	public void validateOutputResultForScreeningRound(Integer idScreeningRound, Float outputResult1,
			Float outputResult2, Float outputResult3, Float dilutionValue1, Float dilutionValue2,
			Float dilutionValue3) {
		addNewValueForScreeningRound(idScreeningRound, outputResult1, dilutionValue1, "Dilution Output", "phages");
		addNewValueForScreeningRound(idScreeningRound, outputResult2, dilutionValue2, "Dilution Output", "phages");
		addNewValueForScreeningRound(idScreeningRound, outputResult3, dilutionValue3, "Dilution Output", "phages");
	}

	@Override
	public void validateMeanOutputResultForScreeningRound(Integer idScreeningRound, Float meanOutput) {
		addNewValueForScreeningRound(idScreeningRound, meanOutput, null, "Mean Output", "phages");
	}

	@Override
	public void validateRatioResultForScreeningRound(Integer idScreeningRound, Float ratio) {
		addNewValueForScreeningRound(idScreeningRound, ratio, null, "Ratio", "wu");
	}

	@Override
	public void validateEnrichmentResultForScreeningRound(Integer idScreeningRound, Float enrichment) {
		addNewValueForScreeningRound(idScreeningRound, enrichment, null, "Enrichment", "wu");
	}

	@Override
	public List<ScientificValue> getAllPHPossibleByBuffer(String nameBuffer) {
		List<ScientificValue> pHs = valueDAO.findPHByNameBufferGroupByPH(nameBuffer);
		return pHs;
	}

	@Override
	public List<ScientificValue> getAllTemperaturePossible() {
		return valueDAO.findAllTemperature();
	}

	@Override
	public ScientificValue getById(Integer idValue) {
		return valueDAO.findById(idValue);
	}

}
