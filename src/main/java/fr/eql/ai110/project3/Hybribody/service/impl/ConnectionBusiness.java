package fr.eql.ai110.project3.Hybribody.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.repository.EmployeeIDAO;
import fr.eql.ai110.project3.Hybribody.service.ConnectionIBusiness;

@Service
public class ConnectionBusiness implements ConnectionIBusiness {

	@Autowired
	private EmployeeIDAO employeeDAO;
	
	@Override
	public Employee authenticate(String mail, String password) {
		Employee employee = new Employee();
		employee = null;
		employee = employeeDAO.findByMail(mail);
		if(employee != null) {
			password += employee.getSalt();
			password = hashedPassword(password);
			employee = employeeDAO.findByMailAndPassword(mail, password);
		}
		return employee;
	}

	@Override
	public String hashedPassword(String password) {
		try
		{
			//choose of hash algorithm with SHA-256
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			//encoding the password into a byte array
			byte[] encodedHash = md.digest(password.getBytes(StandardCharsets.UTF_8));
			//new password string generation using the byte array
			password = new String(encodedHash, StandardCharsets.UTF_8);
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return password;

	}

}
