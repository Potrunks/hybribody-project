package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.ValueCategory;
import fr.eql.ai110.project3.Hybribody.repository.ValueCategoryIDAO;
import fr.eql.ai110.project3.Hybribody.service.ValueCategoryIBusiness;

@Service
public class ValueCategoryBusiness implements ValueCategoryIBusiness {

	@Autowired
	private ValueCategoryIDAO valueCategoryDAO;
	
	@Override
	public List<ValueCategory> getAllSubCategoryByExperimentName(String category, String experiment) {
		return valueCategoryDAO.findByIdParent(valueCategoryDAO.findByIdParentAndName(valueCategoryDAO.findByName(experiment).getId(), category).getId());
	}

}
