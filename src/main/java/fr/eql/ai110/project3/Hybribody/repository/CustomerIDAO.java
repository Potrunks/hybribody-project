package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Customer;

public interface CustomerIDAO extends CrudRepository<Customer, Long> {

	List<Customer> findAll();
	Customer findById(Integer id);
	List<Customer> findByIdNot(Integer id);
}
