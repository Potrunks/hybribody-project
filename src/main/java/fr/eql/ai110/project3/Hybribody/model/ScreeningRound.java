package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "screeninground")
public class ScreeningRound implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idscreeninground")
	private Integer id;
	@Column(name = "nb")
	private Integer nb;
	@Column(name = "datecreation")
	private LocalDate dateCreation;
	@Column(name = "datevalidation")
	private LocalDate dateValidation;
	@Column(name = "datecancelation")
	private LocalDate dateCancelation;
	@Column(name = "valid")
	private boolean valid;
	@Column(name = "validationcause")
	private String validationCause;
	@Column(name = "cancelationCause")
	private String cancelationCause;
	@Column(name = "comment")
	private String comment;
	@OneToMany(mappedBy = "round", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private List<ScientificValue> values;
	@ManyToOne
	@JoinColumn(name="idexperiment")
	private Screening screening;
	@ManyToOne
	@JoinColumn(name = "idpositivetarget")
	private PositiveTarget positiveTarget;
	@ManyToOne
	@JoinColumn(name = "idnegativetarget")
	private NegativeTarget negativeTarget;
	@ManyToMany
	@JoinTable(name = "T_motherplate_round_Associations",
			joinColumns = @JoinColumn(name = "idscreeninground"),
			inverseJoinColumns = @JoinColumn(name = "idexperiment"))
	private List<MotherPlatePicking> motherPlatePickings;
	@OneToMany(mappedBy = "screeningRoundOrigin", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Antibody> antibodiesSelected;
	
	public ScreeningRound() {
		super();
	}

	public ScreeningRound(Integer id, Integer nb, LocalDate dateCreation, LocalDate dateValidation,
			LocalDate dateCancelation, boolean valid, String validationCause, String cancelationCause, String comment,
			List<ScientificValue> values, Screening screening, PositiveTarget positiveTarget,
			NegativeTarget negativeTarget, List<MotherPlatePicking> motherPlatePickings,
			List<Antibody> antibodiesSelected) {
		super();
		this.id = id;
		this.nb = nb;
		this.dateCreation = dateCreation;
		this.dateValidation = dateValidation;
		this.dateCancelation = dateCancelation;
		this.valid = valid;
		this.validationCause = validationCause;
		this.cancelationCause = cancelationCause;
		this.comment = comment;
		this.values = values;
		this.screening = screening;
		this.positiveTarget = positiveTarget;
		this.negativeTarget = negativeTarget;
		this.motherPlatePickings = motherPlatePickings;
		this.antibodiesSelected = antibodiesSelected;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNb() {
		return nb;
	}

	public void setNb(Integer nb) {
		this.nb = nb;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public LocalDate getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(LocalDate dateValidation) {
		this.dateValidation = dateValidation;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getValidationCause() {
		return validationCause;
	}

	public void setValidationCause(String validationCause) {
		this.validationCause = validationCause;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<ScientificValue> getValues() {
		return values;
	}

	public void setValues(List<ScientificValue> values) {
		this.values = values;
	}

	public Screening getScreening() {
		return screening;
	}

	public void setScreening(Screening screening) {
		this.screening = screening;
	}

	public PositiveTarget getPositiveTarget() {
		return positiveTarget;
	}

	public void setPositiveTarget(PositiveTarget positiveTarget) {
		this.positiveTarget = positiveTarget;
	}

	public NegativeTarget getNegativeTarget() {
		return negativeTarget;
	}

	public void setNegativeTarget(NegativeTarget negativeTarget) {
		this.negativeTarget = negativeTarget;
	}

	public List<MotherPlatePicking> getMotherPlatePickings() {
		return motherPlatePickings;
	}

	public void setMotherPlatePickings(List<MotherPlatePicking> motherPlatePickings) {
		this.motherPlatePickings = motherPlatePickings;
	}

	public List<Antibody> getAntibodiesSelected() {
		return antibodiesSelected;
	}

	public void setAntibodiesSelected(List<Antibody> antibodiesSelected) {
		this.antibodiesSelected = antibodiesSelected;
	}

	public LocalDate getDateCancelation() {
		return dateCancelation;
	}

	public void setDateCancelation(LocalDate dateCancelation) {
		this.dateCancelation = dateCancelation;
	}

	public String getCancelationCause() {
		return cancelationCause;
	}

	public void setCancelationCause(String cancelationCause) {
		this.cancelationCause = cancelationCause;
	}

}
