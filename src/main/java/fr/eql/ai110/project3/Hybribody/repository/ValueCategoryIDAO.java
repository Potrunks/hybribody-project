package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.ValueCategory;

public interface ValueCategoryIDAO extends CrudRepository<ValueCategory, Long> {

	ValueCategory findByName(String name);
	ValueCategory findById(Integer id);
	ValueCategory findByIdParentAndName(Integer idParent, String name);
	List<ValueCategory> findByIdParent(Integer idParent);
	List<ValueCategory> findByDilutionValue(Float dilutionValue);
}
