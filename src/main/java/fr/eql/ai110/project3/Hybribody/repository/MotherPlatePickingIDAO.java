package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Project;

public interface MotherPlatePickingIDAO extends CrudRepository<MotherPlatePicking, Long> {

	MotherPlatePicking findById (Integer idExperiment);
	List<MotherPlatePicking> findByProjectsAndDateValidationIsNullOrderByDateCreationDesc (Project project);
	@Query(value = "select xp.* "
			+ "from experiment xp "
			+ "inner join t_experiment_project_associations xpp on xp.idexperiment = xpp.idexperiment "
			+ "inner join project p on xpp.idproject = p.idproject "
			+ "where p.idproject=:idProjectParam "
			+ "and xp.datevalidation is null "
			+ "and xp.category = 'mother_plate_screening' "
			+ "order by xp.datecreation desc", nativeQuery = true)
	List<MotherPlatePicking> findByIdprojectAndDateValidationIsNullOrderByDateCreationDesc (@Param("idProjectParam") Integer idProject);
	long countByDateValidationIsNull();
	long countByNbPlateIsNotNull();
}
