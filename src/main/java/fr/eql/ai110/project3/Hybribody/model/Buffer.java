package fr.eql.ai110.project3.Hybribody.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("buffer")
public class Buffer extends Consumable {

	private static final long serialVersionUID = 1L;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "T_buffer_molecule_Associations",
			joinColumns = @JoinColumn(name = "idconsumable"),
			inverseJoinColumns = @JoinColumn(name = "id_molecule"))
	private List<Molecule> moleculeComposition;
	@OneToMany(mappedBy = "buffer", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ExperimentalCondition> xpCondition;
	@Column(name="application")
	private String application;
	@ManyToOne
	@JoinColumn(name="idvalue")
	private ScientificValue pH;

	public Buffer() {
		super();
	}

	public Buffer(List<Molecule> moleculeComposition, List<ExperimentalCondition> xpCondition, String application,
			ScientificValue pH) {
		super();
		this.moleculeComposition = moleculeComposition;
		this.xpCondition = xpCondition;
		this.application = application;
		this.pH = pH;
	}

	public List<Molecule> getMoleculeComposition() {
		return moleculeComposition;
	}

	public void setMoleculeComposition(List<Molecule> moleculeComposition) {
		this.moleculeComposition = moleculeComposition;
	}

	public List<ExperimentalCondition> getXpCondition() {
		return xpCondition;
	}

	public void setXpCondition(List<ExperimentalCondition> xpCondition) {
		this.xpCondition = xpCondition;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public ScientificValue getpH() {
		return pH;
	}

	public void setpH(ScientificValue pH) {
		this.pH = pH;
	}
	
}
