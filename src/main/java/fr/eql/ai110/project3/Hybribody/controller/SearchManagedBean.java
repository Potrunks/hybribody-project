package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.SubscriptionIBusiness;

@Controller(value = "mbSearch")
@Scope(value = "session")
public class SearchManagedBean implements Serializable {

	private Employee employee;
	private List<Project> projects;
	private String projectNameWanted;
	private String customerSurnameWanted;
	private String customerNameWanted;
	private String selectedXPCategory;
	private Integer selectedParentSubscription;
	private List<Subscription> parentSubscriptions;
	private Integer selectedSubscription;
	private List<Subscription> subscriptions;
	private String selectedStatus = "All";
	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private SubscriptionIBusiness subscriptionBusiness;

	private static final long serialVersionUID = 1L;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}

	public String displayAllProjectsForSearching() {
		String redirect = null;
		reset();
		projects = projectBusiness.getAll();
		parentSubscriptions = subscriptionBusiness.getAllParentSubscription();
		redirect = "/searchProject.xhtml?faces-redirect=true";
		return redirect;
	}

	public void onParentSubscriptionChangeForSubscriptionList() {
		if (selectedParentSubscription != null) {
			subscriptions = subscriptionBusiness.getAllSubSubscription(selectedParentSubscription);
		} else {
			subscriptions = new ArrayList<Subscription>();
		}
	}

	private void reset() {
		projects = new ArrayList<Project>();
		parentSubscriptions = new ArrayList<Subscription>();
		clear();
	}
	
	public void clear() {
		projectNameWanted = "";
		customerSurnameWanted = "";
		customerNameWanted = "";
		selectedXPCategory = "";
		selectedParentSubscription = null;
		selectedSubscription = 0;
		subscriptions = new ArrayList<Subscription>();
		selectedStatus = "All";
	}
	
	public void searchProjectWithCriteriaSelected() {
		projects = projectBusiness.search(projectNameWanted, customerSurnameWanted, customerNameWanted, selectedXPCategory, selectedSubscription, selectedStatus);
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public String getProjectNameWanted() {
		return projectNameWanted;
	}

	public void setProjectNameWanted(String projectNameWanted) {
		if (projectNameWanted == null) {
			projectNameWanted = "";
		}
		this.projectNameWanted = projectNameWanted;
	}

	public String getCustomerSurnameWanted() {
		return customerSurnameWanted;
	}

	public void setCustomerSurnameWanted(String customerSurnameWanted) {
		if (customerSurnameWanted == null) {
			customerSurnameWanted = "";
		}
		this.customerSurnameWanted = customerSurnameWanted;
	}

	public String getCustomerNameWanted() {
		return customerNameWanted;
	}

	public void setCustomerNameWanted(String customerNameWanted) {
		if (customerNameWanted == null) {
			customerNameWanted = "";
		}
		this.customerNameWanted = customerNameWanted;
	}

	public String getSelectedXPCategory() {
		return selectedXPCategory;
	}

	public void setSelectedXPCategory(String selectedXPCategory) {
		if (selectedXPCategory == null) {
			selectedXPCategory = "";
		}
		this.selectedXPCategory = selectedXPCategory;
	}

	public Integer getSelectedParentSubscription() {
		return selectedParentSubscription;
	}

	public void setSelectedParentSubscription(Integer selectedParentSubscription) {
		if (selectedParentSubscription == null) {
			selectedParentSubscription = 0;
		}
		this.selectedParentSubscription = selectedParentSubscription;
	}

	public List<Subscription> getParentSubscriptions() {
		return parentSubscriptions;
	}

	public void setParentSubscriptions(List<Subscription> parentSubscriptions) {
		this.parentSubscriptions = parentSubscriptions;
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}

	public Integer getSelectedSubscription() {
		return selectedSubscription;
	}

	public void setSelectedSubscription(Integer selectedSubscription) {
		if(selectedSubscription == null) {
			selectedSubscription = 0;
		}
		this.selectedSubscription = selectedSubscription;
	}

}
