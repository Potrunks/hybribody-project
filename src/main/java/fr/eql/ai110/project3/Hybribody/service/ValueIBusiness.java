package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.ScientificValue;

public interface ValueIBusiness {

	ScientificValue getByScreeningRoundIdAndValueCategoryName(Integer idRound, String valueCatName);
	Float calculateInputFromNbCloneAndDilution(Integer nbClones, Float dilution);
	Float calculateOutputFromNbCloneAndDilution(Integer nbClones, Float dilution);
	Float calculateMeanInputFromInputResults(Float inputResult1, Float inputResult2);
	Float calculateMeanOutputFromOutputResults(Float outputResult1, Float outputResult2, Float outputResult3);
	Float calculateRatioFromMeanResults(Float meanInput, Float meanOutput);
	Float calculateEnrichmentBetweenRounds(Float newRatio, Float previousRatio);
	Float getRatioValueFromPreviousRound(Integer idScreening, Integer idRound);
	void validateInputResultForScreeningRound(Integer idScreeningRound, Float inputResult1, Float inputResult2, Float dilutionValue1, Float dilutionValue2);
	void validateMeanInputResultForScreeningRound(Integer idScreeningRound, Float meanInput);
	void validateOutputResultForScreeningRound(Integer idScreeningRound, Float outputResult1, Float outputResult2, Float outputResult3, Float dilutionValue1, Float dilutionValue2, Float dilutionValue3);
	void validateMeanOutputResultForScreeningRound(Integer idScreeningRound, Float meanOutput);
	void validateRatioResultForScreeningRound(Integer idScreeningRound, Float ratio);
	void validateEnrichmentResultForScreeningRound(Integer idScreeningRound, Float enrichment);
	List<ScientificValue> getAllPHPossibleByBuffer(String nameBuffer);
	List<ScientificValue> getAllTemperaturePossible();
	ScientificValue getById(Integer idValue);
}
