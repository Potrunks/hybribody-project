package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "targetcategory")
public class TargetCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idtargetcategory")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="idparent")
	private Integer idParent;
	@OneToMany(mappedBy = "targetCategory", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Target> targets;
	
	public TargetCategory() {
		super();
	}

	public TargetCategory(Integer id, String name, Integer idParent, List<Target> targets) {
		super();
		this.id = id;
		this.name = name;
		this.idParent = idParent;
		this.targets = targets;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIdParent() {
		return idParent;
	}

	public void setIdParent(Integer idParent) {
		this.idParent = idParent;
	}

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}
	
}
