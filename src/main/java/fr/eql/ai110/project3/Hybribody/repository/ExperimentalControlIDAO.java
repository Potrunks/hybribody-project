package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;

public interface ExperimentalControlIDAO extends CrudRepository<ExperimentalControl, Long> {

	List<ExperimentalControl> findBycontrolMotherPlate(MotherPlatePicking motherPlatePicking);
	List<ExperimentalControl> findByNameIsNotLike(String name);
	ExperimentalControl findByNameIs(String name);
	ExperimentalControl findById(Integer id);
	List<ExperimentalControl> findByPositionIs(String position);
}
