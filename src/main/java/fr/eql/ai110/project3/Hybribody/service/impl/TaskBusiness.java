package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Task;
import fr.eql.ai110.project3.Hybribody.repository.TaskIDAO;
import fr.eql.ai110.project3.Hybribody.service.TaskIBusiness;

@Service
public class TaskBusiness implements TaskIBusiness {

	@Autowired
	private TaskIDAO taskDAO;

	@Override
	public List<Task> getAllOrderByDateCreationDesc() {
		return taskDAO.findAllOrderByDateCreationDesc();
	}

}
