package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Task;
import fr.eql.ai110.project3.Hybribody.service.EmployeeIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.TaskIBusiness;

@Controller(value = "mbDashBoard")
@Scope(value = "request")
public class DashBoardManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Employee employee;
	private List<Task> tasks;
	private List<Employee> employees;
	private Long projectTotal;
	private Long projectInProgressPercentage;
	private Long projectFinishWithSuccessPercentage;
	private Long projectFinishWithFailPercentage;
	private Long projectCanceledPercentage;
	private DonutChartModel donutModel;

	@Autowired
	private TaskIBusiness taskBusiness;
	@Autowired
	private EmployeeIBusiness employeeBusiness;
	@Autowired
	private ProjectIBusiness projectBusiness;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
		tasks = taskBusiness.getAllOrderByDateCreationDesc();
		employees = employeeBusiness.getAllOrderByDateArrivingAsc();
		calculateProjectStat();
		createDonutModel();
	}

	private void calculateProjectStat() {
		projectTotal = projectBusiness.countAllProjectInTheLab();
		projectInProgressPercentage = projectBusiness.countAllProjectInProgress() * 100 / projectTotal;
		projectFinishWithSuccessPercentage = projectBusiness.countAllProjectClosingWithSuccess() * 100 / projectTotal;
		projectFinishWithFailPercentage = projectBusiness.countAllProjectClosingWithFail() * 100 / projectTotal;
		projectCanceledPercentage = projectBusiness.countAllProjectCanceled() * 100 / projectTotal;
	}

	public void createDonutModel() {
		donutModel = new DonutChartModel();
		ChartData data = new ChartData();

		DonutChartDataSet dataSet = new DonutChartDataSet();
		List<Number> values = new ArrayList<>();
		values.add(projectInProgressPercentage);
		values.add(projectFinishWithSuccessPercentage);
		values.add(projectFinishWithFailPercentage);
		values.add(projectCanceledPercentage);
		dataSet.setData(values);

		List<String> bgColors = new ArrayList<>();
		bgColors.add("rgba(5, 24, 167, 0.8)");
		bgColors.add("rgba(3, 140, 7, 0.8)");
		bgColors.add("rgba(201, 153, 16, 0.8)");
		bgColors.add("rgba(201, 0, 0, 0.8)");
		dataSet.setBackgroundColor(bgColors);

		data.addChartDataSet(dataSet);
		List<String> labels = new ArrayList<>();
		labels.add("In Progress");
		labels.add("Success");
		labels.add("Fail");
		labels.add("Canceled");
		data.setLabels(labels);

		donutModel.setData(data);
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public Long getProjectTotal() {
		return projectTotal;
	}

	public void setProjectTotal(Long projectTotal) {
		this.projectTotal = projectTotal;
	}

	public Long getProjectInProgressPercentage() {
		return projectInProgressPercentage;
	}

	public void setProjectInProgressPercentage(Long projectInProgressPercentage) {
		this.projectInProgressPercentage = projectInProgressPercentage;
	}

	public Long getProjectFinishWithSuccessPercentage() {
		return projectFinishWithSuccessPercentage;
	}

	public void setProjectFinishWithSuccessPercentage(Long projectFinishWithSuccessPercentage) {
		this.projectFinishWithSuccessPercentage = projectFinishWithSuccessPercentage;
	}

	public Long getProjectFinishWithFailPercentage() {
		return projectFinishWithFailPercentage;
	}

	public void setProjectFinishWithFailPercentage(Long projectFinishWithFailPercentage) {
		this.projectFinishWithFailPercentage = projectFinishWithFailPercentage;
	}

	public Long getProjectCanceledPercentage() {
		return projectCanceledPercentage;
	}

	public void setProjectCanceledPercentage(Long projectCanceledPercentage) {
		this.projectCanceledPercentage = projectCanceledPercentage;
	}

	public DonutChartModel getDonutModel() {
		return donutModel;
	}

	public void setDonutModel(DonutChartModel donutModel) {
		this.donutModel = donutModel;
	}

}
