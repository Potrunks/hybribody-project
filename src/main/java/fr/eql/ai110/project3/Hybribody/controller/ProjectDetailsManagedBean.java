package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.SubscriptionIBusiness;

@Controller(value = "mbProjectDetails")
@Scope(value = "session")
public class ProjectDetailsManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Employee employee;
	private Project project;
	private List<Subscription> subscriptions;
	private List<Experiment> experimentsInProgress;
	private List<Experiment> experimentsValidated;
	private List<Project> similarProjects;
	
	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private SubscriptionIBusiness subscriptionBusiness;
	@Autowired
	private ExperimentIBusiness experimentBusiness;
	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}
	
	public String loadProjectDetails(Integer idProject) {
		String redirect = null;
		project = projectBusiness.getByIdProject(idProject);
		subscriptions = subscriptionBusiness.getByIDProject(project);
		experimentsInProgress = experimentBusiness.getAllInProgressByProject(project);
		experimentsValidated = experimentBusiness.getAllValidatedByProject(project);
		similarProjects = projectBusiness.getSimilarProjects(project);
		if(project != null) {
			redirect = "/projectDetails.xhtml?faces-redirect=true";
		} else {
			redirect = "/labProject.xhtml?faces-redirect=false";
		}
		return redirect;
	}
	
	public Integer fetchLastProjectEdited() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		Project projectEdited = (Project) session.getAttribute("projectEdited");
		Integer idProjectEdited = projectEdited.getIdproject();
		projectEdited = null;
		session.setAttribute("projectEdited", projectEdited);
		return idProjectEdited;
	}
	
	public boolean noXPInProgress() {
		boolean result = false;
		if(experimentsInProgress.isEmpty()) {
			result = true;
		}
		return result;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public List<Experiment> getExperimentsInProgress() {
		return experimentsInProgress;
	}

	public void setExperimentsInProgress(List<Experiment> experimentsInProgress) {
		this.experimentsInProgress = experimentsInProgress;
	}

	public List<Experiment> getExperimentsValidated() {
		return experimentsValidated;
	}

	public void setExperimentsValidated(List<Experiment> experimentsValidated) {
		this.experimentsValidated = experimentsValidated;
	}

	public List<Project> getSimilarProjects() {
		return similarProjects;
	}

	public void setSimilarProjects(List<Project> similarProjects) {
		this.similarProjects = similarProjects;
	}

}
