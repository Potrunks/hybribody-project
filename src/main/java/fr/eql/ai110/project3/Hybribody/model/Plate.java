package fr.eql.ai110.project3.Hybribody.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("plate")
public class Plate extends Material {

	private static final long serialVersionUID = 1L;

	@Column(name = "total_well")
	private Integer nbOfTotalWell;
	@OneToMany(mappedBy = "plateUsed", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<MotherPlatePicking> motherPlatesPickings;

	public Plate() {
		super();
	}

	public Plate(Integer nbOfTotalWell, List<MotherPlatePicking> motherPlatesPickings) {
		super();
		this.nbOfTotalWell = nbOfTotalWell;
		this.motherPlatesPickings = motherPlatesPickings;
	}

	public Integer getNbOfTotalWell() {
		return nbOfTotalWell;
	}

	public void setNbOfTotalWell(Integer nbOfTotalWell) {
		this.nbOfTotalWell = nbOfTotalWell;
	}

	public List<MotherPlatePicking> getMotherPlatesPickings() {
		return motherPlatesPickings;
	}

	public void setMotherPlatesPickings(List<MotherPlatePicking> motherPlatesPickings) {
		this.motherPlatesPickings = motherPlatesPickings;
	}

}
