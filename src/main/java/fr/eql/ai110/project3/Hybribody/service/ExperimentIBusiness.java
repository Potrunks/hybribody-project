package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;

public interface ExperimentIBusiness {

	/**
	 * Get the name of the most recent experiment in progress in the data base by project
	 * @param idProject : ID of the project wanted
	 * @return a string with the name of the experiment the most recent in progress in the project
	 */
	public String getTheMostRecentInProgress(Integer idProject);
	/**
	 * Get the name of the most recent experiment validated in the data base by project
	 * @param idProject : ID of the project wanted
	 * @return a string with the name of the experiment the most recent validated in the project
	 */
	public String getTheMostRecentValidated(Integer idProject);
	/**
	 * Get all experiment in progress by project
	 * @param project : project wanted
	 * @return a list of experiment in progress in the selected project
	 */
	public List<Experiment> getAllInProgressByProject(Project project);
	/**
	 * Get all experiment validated by project
	 * @param project : project wanted
	 * @return a list of experiment validated by the selected project
	 */
	public List<Experiment> getAllValidatedByProject(Project project);
	/**
	 * Get the experiment by ID Experiment
	 * @param id : ID of the experiment
	 * @return a experiment by the selected ID
	 */
	public Experiment getById(Integer id);
	/**
	 * Get the category of an experiment with the ID experiment
	 * @param idExperiment : ID of the experiment selected
	 * @return a string contain the category of the selected ID experiment
	 */
	public String getCategory(Integer idExperiment);
	/**
	 * Get the most recent experiment in progress in the data base by project
	 * @param idProject : ID of the project wanted
	 * @return the experiment the most recent in progress in the project
	 */
	public Experiment getTheMostRecentXPInProgress(Integer idProject);
	/**
	 * Get the last experiment created by a category name
	 * @param category : name of the category selected
	 * @return the last experiment created with the category name selected
	 */
	public Experiment getLastXpCreatedByCategoryName(String category);
	/**
	 * Verify if an experiment in progress exist in the selected project
	 * @param idProject : ID of the selected project
	 * @return true if an experiment in progress exist in the selected project
	 */
	public boolean verifyXPInProgressExist(Integer idProject);
	/**
	 * Add a cancel date and a cancelation comment "Project canceled" to all experiments in a selected project
	 * @param project
	 */
	public void cancelAllExperimentsInProgressInProject(Project project);
}
