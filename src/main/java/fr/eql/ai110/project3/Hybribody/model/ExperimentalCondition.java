package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "experimental_condition")
public class ExperimentalCondition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_xp_condition")
	private Integer id;
	@ManyToOne
	@JoinColumn(name="idvalue")
	private ScientificValue temperature;
	@ManyToOne
	@JoinColumn(name="idconsumable")
	private Buffer buffer;
	@ManyToMany
	@JoinTable(name = "T_experiment_xpcondition_Associations",
			joinColumns = @JoinColumn(name = "id_xp_condition"),
			inverseJoinColumns = @JoinColumn(name = "idexperiment"))
	private List<Experiment> experiments;
	@Column(name="application")
	private String application;

	public ExperimentalCondition() {
		super();
	}

	public ExperimentalCondition(Integer id, ScientificValue temperature, Buffer buffer, List<Experiment> experiments,
			String application) {
		super();
		this.id = id;
		this.temperature = temperature;
		this.buffer = buffer;
		this.experiments = experiments;
		this.application = application;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Buffer getBuffer() {
		return buffer;
	}

	public void setBuffer(Buffer buffer) {
		this.buffer = buffer;
	}

	public List<Experiment> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<Experiment> experiments) {
		this.experiments = experiments;
	}

	public ScientificValue getTemperature() {
		return temperature;
	}

	public void setTemperature(ScientificValue temperature) {
		this.temperature = temperature;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

}
