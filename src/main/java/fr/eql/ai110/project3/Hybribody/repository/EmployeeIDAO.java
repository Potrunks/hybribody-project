package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Employee;

public interface EmployeeIDAO extends CrudRepository<Employee, Long> {

	Employee findByMailAndPassword(String mail, String password);
	List<Employee> findByDateArrivingIsNotNullOrderByDateArrivingAsc();
	Employee findByMail(String mail);
}
