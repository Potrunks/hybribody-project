package fr.eql.ai110.project3.Hybribody.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("target")
public class Target extends Consumable {

	private static final long serialVersionUID = 1L;
	
	@ManyToMany
	@JoinTable(name = "T_target_tag_Associations",
			joinColumns = @JoinColumn(name = "idconsumable"),
			inverseJoinColumns = @JoinColumn(name = "idtag"))
	private List<Tag> tags;
	@ManyToOne
	@JoinColumn(name = "idlabel")
	private Label label;
	@ManyToOne
	@JoinColumn(name = "idtargetcategory")
	private TargetCategory targetCategory;
	@OneToMany(mappedBy = "target", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<PositiveTarget> positiveTargets;
	@OneToMany(mappedBy = "target", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<NegativeTarget> negativeTargets;
	
	public Target() {
		super();
	}

	public Target(List<Tag> tags, Label label, TargetCategory targetCategory, List<PositiveTarget> positiveTargets,
			List<NegativeTarget> negativeTargets) {
		super();
		this.tags = tags;
		this.label = label;
		this.targetCategory = targetCategory;
		this.positiveTargets = positiveTargets;
		this.negativeTargets = negativeTargets;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public TargetCategory getTargetCategory() {
		return targetCategory;
	}

	public void setTargetCategory(TargetCategory targetCategory) {
		this.targetCategory = targetCategory;
	}

	public List<PositiveTarget> getPositiveTargets() {
		return positiveTargets;
	}

	public void setPositiveTargets(List<PositiveTarget> positiveTargets) {
		this.positiveTargets = positiveTargets;
	}

	public List<NegativeTarget> getNegativeTargets() {
		return negativeTargets;
	}

	public void setNegativeTargets(List<NegativeTarget> negativeTargets) {
		this.negativeTargets = negativeTargets;
	}
	
}
