package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.service.ConnectionIBusiness;

@Controller(value = "mbConnection")
@Scope(value = "request")
public class ConnectionManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Employee employee;
	private String mail;
	private String password;

	@Autowired
	private ConnectionIBusiness connectionBusiness;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}

	public String connection() {
		String redirect = null;
		employee = connectionBusiness.authenticate(mail, password);
		if (employee != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
			session.setAttribute("employee", employee);
			redirect = "/home.xhtml?faces-redirect=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Mail and/or password incorrect", "Mail and/or password incorrect");
			FacesContext.getCurrentInstance().addMessage("loginForm:loginInput", facesMessage);
			redirect = "/index.xhtml?faces-redirect=false";
		}
		return redirect;
	}

	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.invalidate();
		FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
				"Session disconnected", "Session disconnected");
		FacesContext.getCurrentInstance().addMessage("loginForm:employee-not-connected", facesMessage);
		return "/index.xhtml?faces-redirect=false";
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
