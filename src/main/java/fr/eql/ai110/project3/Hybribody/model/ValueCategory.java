package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "valuecategory")
public class ValueCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idvaluecategory")
	private Integer id;
	@Column(name="idparent")
	private Integer idParent;
	@Column(name="name")
	private String name;
	@ManyToMany
	@JoinTable(name = "T_unity_valuecategory_Associations",
	joinColumns = @JoinColumn(name = "idvaluecategory"),
	inverseJoinColumns = @JoinColumn(name = "idunity"))
	private List<Unity> unities;
	@OneToMany(mappedBy = "category", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ScientificValue> values;
	@Column(name="dilution_value")
	private Float dilutionValue;
	
	public ValueCategory() {
		super();
	}
	

	public ValueCategory(Integer id, Integer idParent, String name, List<Unity> unities, List<ScientificValue> values,
			Float dilutionValue) {
		super();
		this.id = id;
		this.idParent = idParent;
		this.name = name;
		this.unities = unities;
		this.values = values;
		this.dilutionValue = dilutionValue;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdParent() {
		return idParent;
	}

	public void setIdParent(Integer idParent) {
		this.idParent = idParent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Unity> getUnities() {
		return unities;
	}

	public void setUnities(List<Unity> unities) {
		this.unities = unities;
	}

	public List<ScientificValue> getValues() {
		return values;
	}

	public void setValues(List<ScientificValue> values) {
		this.values = values;
	}


	public Float getDilutionValue() {
		return dilutionValue;
	}


	public void setDilutionValue(Float dilutionValue) {
		this.dilutionValue = dilutionValue;
	}
	
}
