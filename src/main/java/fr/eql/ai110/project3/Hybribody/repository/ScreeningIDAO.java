package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.Screening;

public interface ScreeningIDAO extends CrudRepository<Screening, Long> {

	Screening findById(Integer idExperiment);
	@Query(value="SELECT xp.* "
			+ "FROM experiment xp "
			+ "INNER JOIN t_experiment_project_associations xpp ON xp.idexperiment = xpp.idexperiment "
			+ "INNER JOIN screeninground scr ON xp.idexperiment = scr.idexperiment "
			+ "WHERE xp.datevalidation IS NULL "
			+ "AND scr.datevalidation IS NULL "
			+ "AND xpp.idproject=:idProjectParam "
			+ "GROUP BY xp.idexperiment "
			+ "ORDER BY xp.datecreation DESC", nativeQuery = true)
	List<Screening> findInProgressByIdProjectWhereScreeningRoundInProgress(@Param("idProjectParam") Integer idProject);
	@Query(value="select xp.* "
			+ "from experiment xp "
			+ "inner join screeninground scrro on scrro.idexperiment = xp.idexperiment "
			+ "INNER JOIN t_experiment_project_associations xpp ON xpp.idexperiment = xp.idexperiment "
			+ "inner join project p on p.idproject = xpp.idproject "
			+ "where xp.category = 'screening' "
			+ "and xp.datevalidation is not null "
			+ "and xp.valid = 1 "
			+ "and scrro.datevalidation is not null "
			+ "and scrro.valid = 1 "
			+ "and p.idproject = :idProject "
			+ "group by xp.idexperiment", nativeQuery = true)
	List<Screening> findAvailableScreeningsWithAvailableRoundsForMotherPlatePicking(@Param("idProject") Integer idProject);
}