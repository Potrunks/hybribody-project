package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="category")
@Table(name = "consumable")
public class Consumable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idconsumable")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "datecreation")
	private LocalDate dateCreation;
	@Column(name = "comment")
	private String comment;
	@Column(name = "datedestruction")
	private LocalDate dateDestruction;
	@ManyToOne
	@JoinColumn(name = "idemployee")
	private Employee employee;
	// experimental condition
	// stockage
	
	public Consumable() {
		super();
	}

	public Consumable(Integer id, String name, LocalDate dateCreation, String comment, LocalDate dateDestruction,
			Employee employee) {
		super();
		this.id = id;
		this.name = name;
		this.dateCreation = dateCreation;
		this.comment = comment;
		this.dateDestruction = dateDestruction;
		this.employee = employee;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDate getDateDestruction() {
		return dateDestruction;
	}

	public void setDateDestruction(LocalDate dateDestruction) {
		this.dateDestruction = dateDestruction;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
