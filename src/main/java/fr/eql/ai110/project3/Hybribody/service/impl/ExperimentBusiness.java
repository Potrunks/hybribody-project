package fr.eql.ai110.project3.Hybribody.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.repository.ExperimentIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningRoundIDAO;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;

@Service
public class ExperimentBusiness implements ExperimentIBusiness {

	@Autowired
	private ExperimentIDAO experimentDAO;
	@Autowired
	private ScreeningRoundIDAO roundDAO;
	
	@Override
	public String getTheMostRecentInProgress(Integer idProject) {
		return verifyIfTheMostXPIsPresent(experimentDAO.findByMostRecentDateCreation(idProject));
	}
	
	public String verifyIfTheMostXPIsPresent(Experiment experiment) {
		String result = "No Experiment In Progress";
		if(experiment != null) {
			result = experiment.getName();
		}
		return result;
	}

	@Override
	public String getTheMostRecentValidated(Integer idProject) {
		return verifyIfTheMostValidatedIsPresent(experimentDAO.findByMostRecentDateValidation(idProject));
	}
	
	public String verifyIfTheMostValidatedIsPresent(Experiment experiment) {
		String result = "No Experiment Validated";
		if(experiment != null) {
			result = experiment.getName();
		}
		return result;
	}

	@Override
	public List<Experiment> getAllInProgressByProject(Project project) {
		return experimentDAO.findByProjectsAndDateValidationIsNullAndDateCancelationIsNullOrderByDateCreationDesc(project);
	}

	@Override
	public List<Experiment> getAllValidatedByProject(Project project) {
		return experimentDAO.findByProjectsAndDateValidationIsNotNullAndDateCancelationIsNullOrderByDateValidationDesc(project);
	}

	@Override
	public Experiment getById(Integer id) {
		return experimentDAO.findById(id);
	}

	@Override
	public String getCategory(Integer idExperiment) {
		return experimentDAO.findCategoryById(idExperiment);
	}

	@Override
	public Experiment getTheMostRecentXPInProgress(Integer idProject) {
		return experimentDAO.findByMostRecentDateCreation(idProject);
	}

	@Override
	public Experiment getLastXpCreatedByCategoryName(String category) {
		return experimentDAO.findLastCreatedByCategory(category);
	}

	@Override
	public boolean verifyXPInProgressExist(Integer idProject) {
		Experiment xp = experimentDAO.findByMostRecentDateCreation(idProject);
		return xpInProgressExistVerificator(xp);
	}
	
	public boolean xpInProgressExistVerificator(Experiment xpVerificator) {
		boolean exist = false;
		if(xpVerificator != null) {
			exist = true;
		}
		return exist;
	}

	@Override
	public void cancelAllExperimentsInProgressInProject(Project project) {
		List<Experiment> experiments = getAllInProgressByProject(project);
		if(experiments != null) {
			for (Experiment experiment : experiments) {
				experiment.setDateCancelation(LocalDate.now());
				experiment.setCancelationComment("Project canceled");
				experimentDAO.save(experiment);
				if(getCategory(experiment.getId()).equals("screening")) {
					List<ScreeningRound> rounds = roundDAO.findByScreeningAndDateValidationIsNullOrderByNbAsc((Screening) experiment);
					for (ScreeningRound round : rounds) {
						round.setDateCancelation(LocalDate.now());
						round.setCancelationCause("Project canceled");
						roundDAO.save(round);
					}
				}
			}
		}
	}

}
