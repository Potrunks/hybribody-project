package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;

@Controller(value="mbLabProjectsInProgress")
@Scope(value="request")
public class LabProjectsInProgressManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Employee employee;
	private List<Project> projectsInProgress;
	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private ExperimentIBusiness experimentBusiness;
	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}
	
	@PostConstruct
	public void displayProjectsInProgress() {
		projectsInProgress = projectBusiness.getProjectsInProgress();
	}
	
	public String displayMostRecentXpInProgress(Integer idProject) {
		String xp = experimentBusiness.getTheMostRecentInProgress(idProject);
		return xp;
	}
	
	public String displayMostRecentXpValidated(Integer idProject) {
		String xp = experimentBusiness.getTheMostRecentValidated(idProject);
		return xp;
	}
	
	public boolean verifyXPInProgressExist(Integer idProject) {
		boolean exist = false;
		exist = experimentBusiness.verifyXPInProgressExist(idProject);
		return exist;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Project> getProjectsInProgress() {
		return projectsInProgress;
	}

	public void setProjectsInProgress(List<Project> projectsInProgress) {
		this.projectsInProgress = projectsInProgress;
	}
	
}
