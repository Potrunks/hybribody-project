package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;
import fr.eql.ai110.project3.Hybribody.repository.SubscriptionIDAO;
import fr.eql.ai110.project3.Hybribody.service.SubscriptionIBusiness;

@Service
public class SubscriptionBusiness implements SubscriptionIBusiness {

	@Autowired
	private SubscriptionIDAO subscriptionDAO;

	@Override
	public List<Subscription> getByIDProject(Project project) {
		return subscriptionDAO.findByProjects(project);
	}

	@Override
	public List<Subscription> getAllParentSubscription() {
		return subscriptionDAO.findByIdSubscriptionParentIsNull();
	}

	@Override
	public List<Subscription> getAllSubSubscription(Integer idSubscriptionParent) {
		return subscriptionDAO.findByIdSubscriptionParent(idSubscriptionParent);
	}

	@Override
	public List<Integer> getIdSubscriptionsByIdProjectAndIdParentSubscription(Integer idProject,
			Integer idParentSubscription) {
		return subscriptionDAO.findIdSubscriptionsByIdProjectAndIdParentSubscription(idProject, idParentSubscription);
	}

}
