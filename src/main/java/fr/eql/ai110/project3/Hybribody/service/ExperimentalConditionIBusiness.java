package fr.eql.ai110.project3.Hybribody.service;

import fr.eql.ai110.project3.Hybribody.model.Buffer;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalCondition;
import fr.eql.ai110.project3.Hybribody.model.ScientificValue;

public interface ExperimentalConditionIBusiness {

	/**
	 * Verify if a experimental condition exist by the temperature value, the buffer and the application.
	 * If the experimental condition don't exist, the method create a new experimental condition in the data base
	 * @param temperature : temperature value wanted
	 * @param buffer : buffer wanted
	 * @param application : application wanted
	 * @return a experimental condition matched with the parameters input or created with the parameters
	 */
	public ExperimentalCondition verifyIfExistOrCreate(ScientificValue temperature, Buffer buffer, String application);
}
