package fr.eql.ai110.project3.Hybribody.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Antibody;
import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalCondition;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.model.Task;
import fr.eql.ai110.project3.Hybribody.repository.AntibodyIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ExperimentalControlIDAO;
import fr.eql.ai110.project3.Hybribody.repository.MotherPlatePickingIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ProjectIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningRoundIDAO;
import fr.eql.ai110.project3.Hybribody.repository.TaskIDAO;
import fr.eql.ai110.project3.Hybribody.service.MotherPlatePickingIBusiness;

@Service
public class MotherPlatePickingBusiness implements MotherPlatePickingIBusiness {

	private static final Integer maxWellAvailable = 96;
	private static final String[] charRow = new String[] { "A", "B", "C", "D", "E", "F", "G", "H" };
	
	@Autowired
	private MotherPlatePickingIDAO motherPlatePickingDAO;
	@Autowired
	private ExperimentalControlIDAO experimentalControlDAO;
	@Autowired
	private ScreeningRoundIDAO screeningRoundDAO;
	@Autowired
	private AntibodyIDAO antibodyDAO;
	@Autowired
	private TaskIDAO taskDAO;
	@Autowired
	private ProjectIDAO projectDAO;

	@Override
	public MotherPlatePicking getById(Integer idExperiment) {
		return motherPlatePickingDAO.findById(idExperiment);
	}

	@Override
	public List<MotherPlatePicking> getAllInProgressByProject(Project project) {
		return motherPlatePickingDAO.findByProjectsAndDateValidationIsNullOrderByDateCreationDesc(project);
	}

	@Override
	public List<MotherPlatePicking> getAllInProgressByIdProject(Integer idProject) {
		return motherPlatePickingDAO.findByIdprojectAndDateValidationIsNullOrderByDateCreationDesc(idProject);
	}

	@Override
	public void validate(Integer idExperiment, boolean isSuccess, String failureCause) {
		MotherPlatePicking plate = updateMotherPlate(idExperiment, isSuccess, failureCause);
		if (plate.isValid() == true) {
			List<ScreeningRound> rounds = screeningRoundDAO.findByMotherPlatePickingsOrderByNbAsc(plate);
			List<ExperimentalControl> controls = experimentalControlDAO.findBycontrolMotherPlate(plate);
			Integer nbOfClonePickedPerRound = maxWellAvailable / rounds.size();
			Integer currentRow = 0;
			Integer currentColumn = 1;
			String well = "";
			boolean positionIsOK = true;
			for (ScreeningRound round : rounds) {
				for (int i = 1; i <= nbOfClonePickedPerRound; i++) {
					well = charRow[currentRow] + currentColumn.toString();
					positionIsOK = true;
					for (ExperimentalControl control : controls) {
						if (control.getPosition().equals(well)) {
							positionIsOK = false;
							break;
						}
					}
					if (positionIsOK == true) {
						generateAb(round, plate, well);
					}
					if (currentRow == charRow.length - 1) {
						currentRow = 0;
						currentColumn++;
					} else {
						currentRow++;
					}
				}
			}
		}
	}
	
	public void generateAb(ScreeningRound round, MotherPlatePicking plate, String well) {
		Antibody ab = new Antibody();
		ab.setDateCreation(LocalDate.now());
		String library = round.getScreening().getLibrary().getName();
		String nbPlate = plate.getNbPlate();
		ab.setName(library + "_" + nbPlate + "_" + well);
		ab.setPosition(well);
		ab.setMotherPlatePicking(plate);
		ab.setScreeningRoundOrigin(round);
		antibodyDAO.save(ab);
	}
	
	private MotherPlatePicking updateMotherPlate(Integer idExperiment, boolean isSuccess, String failureCause) {
		MotherPlatePicking plate = motherPlatePickingDAO.findById(idExperiment);
		plate.setValid(isSuccess);
		plate.setValidationComment(failureCause);
		plate.setDateValidation(LocalDate.now());
		motherPlatePickingDAO.save(plate);
		Task task = new Task();
		task.setDateCreation(LocalDateTime.now());
		task.setDateFormat(task.getDateCreation());
		task.setEmployee(plate.getEmployee());
		Project project = projectDAO.findByExperiments(plate);
		task.setTaskDone("Project " + project.getName() + " - " + plate.getName() + " validated");
		taskDAO.save(task);
		return plate;
	}

	@Override
	public boolean verifyMotherPlateInProgressExist() {
		boolean exist = false;
		if(motherPlatePickingDAO.countByDateValidationIsNull() != 0) {
			exist = true;
		}
		return exist;
	}

	@Override
	public void create(List<ScreeningRound> pickedRounds, List<ExperimentalControl> controls, Employee employee,
			List<Project> projects, List<ExperimentalCondition> xpConditions) {
		MotherPlatePicking motherPlatePicking = new MotherPlatePicking(pickedRounds, controls, null, null, null);
		motherPlatePicking.setEmployee(employee);
		motherPlatePicking.setProjects(projects);
		motherPlatePicking.setNbPlate("MP" + (motherPlatePickingDAO.countByNbPlateIsNotNull() + 1));
		motherPlatePicking.setName("Mother Plate " + motherPlatePicking.getNbPlate());
		motherPlatePicking.setDateCreation(LocalDate.now());
		motherPlatePicking.setValid(false);
		motherPlatePicking = motherPlatePickingDAO.save(motherPlatePicking);
		motherPlatePicking.setXpConditions(xpConditions);
		motherPlatePickingDAO.save(motherPlatePicking);
		taskMotherPlatePickingCreated(employee, projects, motherPlatePicking);
	}
	
	private void taskMotherPlatePickingCreated(Employee employee, List<Project> projects, MotherPlatePicking motherPlatePicking) {
		Task task = new Task();
		task.setDateCreation(LocalDateTime.now());
		task.setDateFormat(task.getDateCreation());
		task.setEmployee(employee);
		task.setTaskDone("Project " + projects.get(0).getName() + " - " + motherPlatePicking.getName() + " created");
		taskDAO.save(task);
	}
	
}
