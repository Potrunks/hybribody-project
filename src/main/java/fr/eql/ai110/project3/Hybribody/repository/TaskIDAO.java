package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Task;

public interface TaskIDAO extends CrudRepository<Task, Long> {

	@Query(value="SELECT * FROM task order by datecreation desc limit 15", nativeQuery = true)
	List<Task> findAllOrderByDateCreationDesc();
}
