package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;

public interface SubscriptionIBusiness {
	
	public List<Subscription> getByIDProject(Project project);
	public List<Subscription> getAllParentSubscription();
	public List<Subscription> getAllSubSubscription(Integer idSubscriptionParent);
	public List<Integer> getIdSubscriptionsByIdProjectAndIdParentSubscription(Integer idProject, Integer idParentSubscription);
}
