package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Antibody;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;

public interface AntibodyIBusiness {

	/**
	 * Method allow to get all antibodies picked by one Mother Plate Picking
	 * @param motherPlatePicking : Who contain the antibodies wanted
	 * @return List of antibodies from this Mother Plate Picking
	 */
	List<Antibody> getAntibodiesPicked(MotherPlatePicking motherPlatePicking);
}
