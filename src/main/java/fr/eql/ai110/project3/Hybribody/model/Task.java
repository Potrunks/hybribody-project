package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "task")
public class Task implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idtask")
	private Integer id;
	@Column(name = "taskdone")
	private String taskDone;
	@Column(name = "datecreation")
	private LocalDateTime dateCreation;
	@ManyToOne
	@JoinColumn(name = "idemployee")
	private Employee employee;
	@Column(name="date_format")
	private String dateFormat;
	
	public Task() {
		super();
	}

	public Task(Integer id, String taskDone, LocalDateTime dateCreation, Employee employee, String dateFormat) {
		super();
		this.id = id;
		this.taskDone = taskDone;
		this.dateCreation = dateCreation;
		this.employee = employee;
		this.dateFormat = dateFormat;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskDone() {
		return taskDone;
	}

	public void setTaskDone(String taskDone) {
		
		this.taskDone = taskDone;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public LocalDateTime getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(LocalDateTime dateToFormat) {
		String dateFormat = new String();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		dateFormat = dateToFormat.format(formatter);
		this.dateFormat = dateFormat;
	}

}
