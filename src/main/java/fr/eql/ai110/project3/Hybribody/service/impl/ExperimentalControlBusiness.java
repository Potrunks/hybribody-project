package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.repository.ExperimentalControlIDAO;
import fr.eql.ai110.project3.Hybribody.service.ExperimentalControlIBusiness;

@Service
public class ExperimentalControlBusiness implements ExperimentalControlIBusiness {

	@Autowired
	private ExperimentalControlIDAO experimentalControlDAO;

	@Override
	public List<ExperimentalControl> getControls(MotherPlatePicking motherPlatePicking) {
		return experimentalControlDAO.findBycontrolMotherPlate(motherPlatePicking);
	}

	@Override
	public List<ExperimentalControl> getControlsWithoutOneControlOfChoice(String controlDidNotWant) {
		return experimentalControlDAO.findByNameIsNotLike(controlDidNotWant);
	}

	@Override
	public String getNameByID(Integer idXpControl) {
		return experimentalControlDAO.findById(idXpControl).getTargetRecognize();
	}

	@Override
	public String getPositionByID(Integer idXpControl) {
		return experimentalControlDAO.findById(idXpControl).getPosition();
	}

	@Override
	public List<ExperimentalControl> getControlsByPosition(String position) {
		return experimentalControlDAO.findByPositionIs(position);
	}

	@Override
	public List<ExperimentalControl> getByMultipleIdXPControls(List<Integer> idXPControls) {
		List<ExperimentalControl> xpControls = new ArrayList<ExperimentalControl>();
		for (Integer idXPControl : idXPControls) {
			if (idXPControl != null) {
				xpControls.add(experimentalControlDAO.findById(idXPControl));
			}
		}
		return xpControls;
	}

	@Override
	public ExperimentalControl getByName(String name) {
		return experimentalControlDAO.findByNameIs(name);
	}

}
