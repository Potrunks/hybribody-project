package fr.eql.ai110.project3.Hybribody.service;

import fr.eql.ai110.project3.Hybribody.model.Employee;

public interface ConnectionIBusiness {

	/**
	 * Allow to verify the input mail and password by the user in order to allow a connection to this application
	 * @param mail : input by the user
	 * @param password : input by the user
	 * @return the employee matched with the mail and the password input by the user
	 */
	public Employee authenticate(String mail, String password);
	/**
	 * Allow to hashed a string. In this case, the string is the password input by the user
	 * @param password : input by the user
	 * @return the password hashed
	 */
	public String hashedPassword(String password);
}
