package fr.eql.ai110.project3.Hybribody.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Buffer;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalCondition;
import fr.eql.ai110.project3.Hybribody.model.ScientificValue;
import fr.eql.ai110.project3.Hybribody.repository.ExperimentalConditionIDAO;
import fr.eql.ai110.project3.Hybribody.service.ExperimentalConditionIBusiness;

@Service
public class ExperimentalConditionBusiness implements ExperimentalConditionIBusiness {
	
	@Autowired
	private ExperimentalConditionIDAO xpConditionDAO;

	@Override
	public ExperimentalCondition verifyIfExistOrCreate(ScientificValue temperature, Buffer buffer, String application) {
		ExperimentalCondition verifiedXpCondition = xpConditionDAO.findByTemperatureAndBufferAndApplication(temperature, buffer, application);
		return xpConditionVerificator(verifiedXpCondition, temperature, buffer, application);
	}
	
	public ExperimentalCondition xpConditionVerificator(ExperimentalCondition xpConditionToVerified, ScientificValue temperature, Buffer buffer, String application) {
		if(xpConditionToVerified == null) {
			xpConditionToVerified = new ExperimentalCondition(null, temperature, buffer, null, application);
			xpConditionDAO.save(xpConditionToVerified);
		}
		return xpConditionToVerified;
	}

}
