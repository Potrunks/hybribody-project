package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Buffer;
import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalCondition;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.model.ScientificValue;
import fr.eql.ai110.project3.Hybribody.service.BufferIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ExperimentalConditionIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ExperimentalControlIBusiness;
import fr.eql.ai110.project3.Hybribody.service.MotherPlatePickingIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ScreeningIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ScreeningRoundIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ValueIBusiness;

@Controller(value = "mbXPCreator")
@Scope(value = "session")
public class ExperimentCreatorManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Employee employee;
	private List<Project> projects;
	private Integer selectedProject;
	private Integer selectedScreening;
	private List<Screening> screenings;
	private List<ScreeningRound> rounds;
	private Integer selectedRound1;
	private Integer selectedRound2;
	private Integer selectedControl1;
	private Integer selectedControl2;
	private List<ExperimentalControl> xpControlsA6;
	private List<ExperimentalControl> xpControlsB6;
	private String targetRecognize1;
	private String positionControl1;
	private String targetRecognize2;
	private String positionControl2;
	private Integer selectedBuffer;
	private Float selectedPH;
	private List<ScientificValue> pHs;
	private Integer selectedTemp;
	private List<ScientificValue> temperatures;
	private List<Buffer> buffers;
	private Experiment xp;

	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private ScreeningRoundIBusiness roundBusiness;
	@Autowired
	private ScreeningIBusiness screeningBusiness;
	@Autowired
	private ExperimentalControlIBusiness xpControlBusiness;
	@Autowired
	private BufferIBusiness bufferBusiness;
	@Autowired
	private ValueIBusiness valueBusiness;
	@Autowired
	private MotherPlatePickingIBusiness motherPlatePickingBusiness;
	@Autowired
	private ExperimentalConditionIBusiness xpConditionBusiness;
	@Autowired
	private ExperimentIBusiness experimentBusiness;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}

	// Region for Mother Plate Experiment
	public String displayProjectsWithAvailableScreeningWhoContainAvailableRound() {
		String redirect = null;
		loadingForMotherPlatePicking();
		redirect = "/newMotherPlatePicking.xhtml?faces-redirect=true";
		return redirect;
	}
	
	public void loadingForMotherPlatePicking() {
		reset();
		projects = projectBusiness.getAllByAvailableScreeningWithAvailableRound();
		temperatures = valueBusiness.getAllTemperaturePossible();
		xpControlsA6 = xpControlBusiness.getControlsByPosition("A6");
		xpControlsB6 = xpControlBusiness.getControlsByPosition("B6");
	}

	public boolean verifyRoundEligibleForMotherPlatePicking() {
		boolean isExist = projectBusiness.verifyAvailableScreeningWithAvailableRound();
		return isExist;
	}
	
	public boolean verifyAvailableScreeningWithAvailableRoundByIdProject(Integer idProject) {
		boolean isExist = projectBusiness.verifyAvailableScreeningWithAvailableRoundByIdProject(idProject);
		return isExist;
	}

	public void onProjectChangeForScreeningEligibleForMotherPlatePicking() {
		if (selectedProject != null) {
			screenings = screeningBusiness.getAllEligibleScreeningForMotherPlatePicking(selectedProject);
			selectedScreening = null;
		} else {
			screenings = new ArrayList<Screening>();
			selectedScreening = null;
			buffers = new ArrayList<Buffer>();
			selectedBuffer = null;
			selectedPH = null;
		}
	}

	public void onScreeningChangeForRoundEligibleForMotherPlatePicking() {
		if (selectedScreening != null) {
			rounds = roundBusiness.getRoundsValidatedAndSuccess(screeningBusiness.getById(selectedScreening));
		} else {
			rounds = new ArrayList<ScreeningRound>();
			selectedRound1 = null;
			selectedRound2 = null;
		}
	}

	public void onControl1ChangeForTarget1() {
		if (selectedControl1 != null) {
			targetRecognize1 = xpControlBusiness.getNameByID(selectedControl1);
			positionControl1 = xpControlBusiness.getPositionByID(selectedControl1);
		} else {
			targetRecognize1 = "";
			positionControl1 = "";
		}
	}

	public void onControl1ChangeForTarget2() {
		if (selectedControl2 != null) {
			targetRecognize2 = xpControlBusiness.getNameByID(selectedControl2);
			positionControl2 = xpControlBusiness.getPositionByID(selectedControl2);
		} else {
			targetRecognize2 = "";
			positionControl2 = "";
		}
	}

	public void onConditionChangeForPH() {
		if (selectedBuffer != null) {
			pHs = valueBusiness.getAllPHPossibleByBuffer(bufferBusiness.getByIdBuffer(selectedBuffer).getName());
		} else {
			pHs = new ArrayList<ScientificValue>();
			selectedPH = null;
		}
	}

	public void onScreeningChangeForBuffer() {
		if (selectedScreening != null) {
			buffers = bufferBusiness
					.getAllByScreeningType(screeningBusiness.getById(selectedScreening).getCategory().getName());
		} else {
			buffers = new ArrayList<Buffer>();
			selectedBuffer = null;
			selectedPH = null;
		}
	}

	public String createMotherPlatePicking() {
		String redirect = null;
		redirect = verifyRequiredFieldForMotherPlatePickingCreation(redirect);
		if (redirect == null) {
			List<Project> projects = new ArrayList<Project>();
			projects.add(projectBusiness.getByIdProject(selectedProject));
			List<Integer> idRounds = new ArrayList<Integer>();
			idRounds.add(selectedRound1);
			idRounds.add(selectedRound2);
			List<ScreeningRound> rounds = roundBusiness.getByMultipleIdScreeningRound(idRounds);
			List<Integer> idXPControls = new ArrayList<Integer>();
			idXPControls.add(selectedControl1);
			idXPControls.add(selectedControl2);
			List<ExperimentalControl> controls = xpControlBusiness.getByMultipleIdXPControls(idXPControls);
			controls.add(xpControlBusiness.getByName("Blank"));
			ScientificValue temperature = valueBusiness.getById(selectedTemp);
			Buffer buffer = bufferBusiness.getByIdBuffer(selectedBuffer);
			buffer = bufferBusiness.getByNameAndPH(buffer.getName(), selectedPH);
			ExperimentalCondition xpCondition = xpConditionBusiness.verifyIfExistOrCreate(temperature, buffer,
					"Growing Condition");
			List<ExperimentalCondition> xpConditions = new ArrayList<ExperimentalCondition>();
			xpConditions.add(xpCondition);
			motherPlatePickingBusiness.create(rounds, controls, employee, projects, xpConditions);
			xp = experimentBusiness.getLastXpCreatedByCategoryName("mother_plate_screening");
			redirect = "/newMotherPlatePickingSuccess.xhtml?faces-redirect=true";
		}
		return redirect;
	}
	
	private String verifyRequiredFieldForMotherPlatePickingCreation(String redirect) {
		if (selectedProject == null || selectedScreening == null || selectedRound1 == null
				|| selectedTemp == null || selectedBuffer == null || employee == null || selectedPH == null || selectedRound1 == selectedRound2) {
			if (selectedProject == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Project selected");
				fm.setDetail("No Project selected");
				FacesContext.getCurrentInstance().addMessage("form-main:projectList", fm);
			}
			if (selectedScreening == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Screening selected");
				fm.setDetail("No Screening selected");
				FacesContext.getCurrentInstance().addMessage("form-main:screeningList", fm);
			}
			if (selectedRound1 == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("One selected Round is required");
				fm.setDetail("One selected Round is required");
				FacesContext.getCurrentInstance().addMessage("form-main:roundList1", fm);
			}
			if (selectedRound1 == selectedRound2 && selectedRound1 != null && selectedRound2 != null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("Only one or different round can be selected");
				fm.setDetail("Only one or different round can be selected");
				FacesContext.getCurrentInstance().addMessage("form-main:roundList1", fm);
			}
			if (selectedTemp == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Temperature selected");
				fm.setDetail("No Temperature selected");
				FacesContext.getCurrentInstance().addMessage("form-main:tempList", fm);
			}
			if (selectedBuffer == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No Buffer selected");
				fm.setDetail("No Buffer selected");
				FacesContext.getCurrentInstance().addMessage("form-main:bufferList", fm);
			}
			if (selectedPH == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("No pH selected");
				fm.setDetail("No pH selected");
				FacesContext.getCurrentInstance().addMessage("form-main:pHList", fm);
			}
			if (employee == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("Session disconnected");
				fm.setDetail("Session disconnected");
				FacesContext.getCurrentInstance().addMessage("loginForm:employee-not-connected", fm);
				return redirect = "/index.xhtml?faces-redirect=false";
			}
			redirect = "/newMotherPlatePicking.xhtml?faces-redirect=false";
		}
		return redirect;
	}
	
	// End Region

	private void reset() {
		projects = new ArrayList<Project>();
		selectedProject = null;
		selectedScreening = null;
		screenings = new ArrayList<Screening>();
		rounds = new ArrayList<ScreeningRound>();
		selectedRound1 = null;
		selectedRound2 = null;
		selectedControl1 = null;
		xpControlsA6 = new ArrayList<ExperimentalControl>();
		xpControlsB6 = new ArrayList<ExperimentalControl>();
		targetRecognize1 = "";
		positionControl1 = "";
		targetRecognize2 = "";
		positionControl2 = "";
		selectedControl2 = null;
		selectedBuffer = null;
		selectedPH = null;
		selectedTemp = null;
		buffers = new ArrayList<Buffer>();
		pHs = new ArrayList<ScientificValue>();
		temperatures = new ArrayList<ScientificValue>();
		xp = new Experiment();
	}
	
	public boolean verifyProjectInProgressExist() {
		boolean result = false;
		if(projectBusiness.countAllProjectInProgress() > 0) {
			result = true;
		}
		return result;
	}
	
	public String displayScreeningPhageDisplayEditForm() {
		String redirect = null;
		// liste de projets en cours
		// Liste de banque utilisable pour des screening phage display
		return redirect;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getSelectedProject() {
		return selectedProject;
	}

	public void setSelectedProject(Integer selectedProject) {
		this.selectedProject = selectedProject;
	}

	public ProjectIBusiness getProjectBusiness() {
		return projectBusiness;
	}

	public void setProjectBusiness(ProjectIBusiness projectBusiness) {
		this.projectBusiness = projectBusiness;
	}

	public Integer getSelectedScreening() {
		return selectedScreening;
	}

	public void setSelectedScreening(Integer selectedScreening) {
		this.selectedScreening = selectedScreening;
	}

	public List<Screening> getScreenings() {
		return screenings;
	}

	public void setScreenings(List<Screening> screenings) {
		this.screenings = screenings;
	}

	public List<ScreeningRound> getRounds() {
		return rounds;
	}

	public void setRounds(List<ScreeningRound> rounds) {
		this.rounds = rounds;
	}

	public Integer getSelectedRound1() {
		return selectedRound1;
	}

	public void setSelectedRound1(Integer selectedRound1) {
		this.selectedRound1 = selectedRound1;
	}

	public Integer getSelectedRound2() {
		return selectedRound2;
	}

	public void setSelectedRound2(Integer selectedRound2) {
		this.selectedRound2 = selectedRound2;
	}

	public Integer getSelectedControl1() {
		return selectedControl1;
	}

	public void setSelectedControl1(Integer selectedControl1) {
		this.selectedControl1 = selectedControl1;
	}

	public String getTargetRecognize1() {
		return targetRecognize1;
	}

	public void setTargetRecognize1(String targetRecognize1) {
		if (targetRecognize1 == null) {
			targetRecognize1 = "";
		}
		this.targetRecognize1 = targetRecognize1;
	}

	public String getPositionControl1() {
		return positionControl1;
	}

	public void setPositionControl1(String positionControl1) {
		if (positionControl1 == null) {
			positionControl1 = "";
		}
		this.positionControl1 = positionControl1;
	}

	public Integer getSelectedControl2() {
		return selectedControl2;
	}

	public void setSelectedControl2(Integer selectedControl2) {
		this.selectedControl2 = selectedControl2;
	}

	public String getTargetRecognize2() {
		return targetRecognize2;
	}

	public void setTargetRecognize2(String targetRecognize2) {
		this.targetRecognize2 = targetRecognize2;
	}

	public String getPositionControl2() {
		return positionControl2;
	}

	public void setPositionControl2(String positionControl2) {
		this.positionControl2 = positionControl2;
	}

	public List<Buffer> getBuffers() {
		return buffers;
	}

	public void setBuffers(List<Buffer> buffers) {
		this.buffers = buffers;
	}

	public Integer getSelectedBuffer() {
		return selectedBuffer;
	}

	public void setSelectedBuffer(Integer selectedBuffer) {
		this.selectedBuffer = selectedBuffer;
	}

	public List<ScientificValue> getpHs() {
		return pHs;
	}

	public void setpHs(List<ScientificValue> pHs) {
		this.pHs = pHs;
	}

	public Integer getSelectedTemp() {
		return selectedTemp;
	}

	public void setSelectedTemp(Integer selectedTemp) {
		this.selectedTemp = selectedTemp;
	}

	public List<ScientificValue> getTemperatures() {
		return temperatures;
	}

	public void setTemperatures(List<ScientificValue> temperatures) {
		this.temperatures = temperatures;
	}

	public List<ExperimentalControl> getXpControlsA6() {
		return xpControlsA6;
	}

	public void setXpControlsA6(List<ExperimentalControl> xpControlsA6) {
		this.xpControlsA6 = xpControlsA6;
	}

	public List<ExperimentalControl> getXpControlsB6() {
		return xpControlsB6;
	}

	public void setXpControlsB6(List<ExperimentalControl> xpControlsB6) {
		this.xpControlsB6 = xpControlsB6;
	}

	public Float getSelectedPH() {
		return selectedPH;
	}

	public void setSelectedPH(Float selectedPH) {
		this.selectedPH = selectedPH;
	}

	public Experiment getXp() {
		return xp;
	}

	public void setXp(Experiment xp) {
		this.xp = xp;
	}

}
