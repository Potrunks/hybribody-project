package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idproject")
	private Integer idproject;
	@Column(name="name")
	private String name;
	@Column(name="datecreation")
	private LocalDate dateCreation;
	@Column(name="dateclosing")
	private LocalDate dateClosing;
	@Column(name="datecancelation")
	private LocalDate dateCancelation;
	@Column(name="comment")
	private String comment;
	@Column(name="success")
	private boolean success;
	@Column(name="cancelationcause")
	private String cancelationCause;
	@Column(name="closingcomment")
	private String closingComment;
	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable( name = "T_subscription_project_Associations",
                joinColumns = @JoinColumn(name="idproject"),
                inverseJoinColumns = @JoinColumn( name = "idsubscription" ) )
	private List<Subscription> subscriptions;
	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinTable(name = "T_experiment_project_Associations",
				joinColumns = @JoinColumn(name = "idproject"),
				inverseJoinColumns = @JoinColumn(name = "idexperiment"))
	private List<Experiment> experiments;
	@ManyToOne
	@JoinColumn(name = "idcustomer")
	private Customer customer;
	
	public Project() {
		super();
	}

	public Project(Integer idproject, String name, LocalDate dateCreation, LocalDate dateClosing,
			LocalDate dateCancelation, String comment, boolean success, String cancelationCause, String closingComment,
			List<Subscription> subscriptions, List<Experiment> experiments, Customer customer) {
		super();
		this.idproject = idproject;
		this.name = name;
		this.dateCreation = dateCreation;
		this.dateClosing = dateClosing;
		this.dateCancelation = dateCancelation;
		this.comment = comment;
		this.success = success;
		this.cancelationCause = cancelationCause;
		this.closingComment = closingComment;
		this.subscriptions = subscriptions;
		this.experiments = experiments;
		this.customer = customer;
	}

	public Integer getIdproject() {
		return idproject;
	}

	public void setIdproject(Integer idproject) {
		this.idproject = idproject;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public LocalDate getDateClosing() {
		return dateClosing;
	}

	public void setDateClosing(LocalDate dateClosing) {
		this.dateClosing = dateClosing;
	}

	public LocalDate getDateCancelation() {
		return dateCancelation;
	}

	public void setDateCancelation(LocalDate dateCancelation) {
		this.dateCancelation = dateCancelation;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCancelationCause() {
		return cancelationCause;
	}

	public void setCancelationCause(String cancelationCause) {
		this.cancelationCause = cancelationCause;
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public List<Experiment> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<Experiment> experiments) {
		this.experiments = experiments;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getClosingComment() {
		return closingComment;
	}

	public void setClosingComment(String closingComment) {
		this.closingComment = closingComment;
	}
	
}
