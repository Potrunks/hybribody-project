package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idcustomer")
	private Integer id;
	@Column(name="surname")
	private String surname;
	@Column(name="name")
	private String name;
	@Column(name="laboratoryname")
	private String laboratoryName;
	@Column(name="mail")
	private String mail;
	@Column(name="phonenumber")
	private String phoneNumber;
	@Column(name="dateregistration")
	private LocalDate dateRegistration;
	@Column(name="nbofstreet")
	private String nbOfStreet;
	@Column(name="streetname")
	private String streetName;
	@Column(name="complement")
	private String complement;
	@OneToMany(mappedBy = "customer", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Project> projects;
	
	public Customer() {
		super();
	}

	public Customer(Integer id, String surname, String name, String laboratoryName, String mail, String phoneNumber,
			LocalDate dateRegistration, String nbOfStreet, String streetName, String complement,
			List<Project> projects) {
		super();
		this.id = id;
		this.surname = surname;
		this.name = name;
		this.laboratoryName = laboratoryName;
		this.mail = mail;
		this.phoneNumber = phoneNumber;
		this.dateRegistration = dateRegistration;
		this.nbOfStreet = nbOfStreet;
		this.streetName = streetName;
		this.complement = complement;
		this.projects = projects;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLaboratoryName() {
		return laboratoryName;
	}

	public void setLaboratoryName(String laboratoryName) {
		this.laboratoryName = laboratoryName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public LocalDate getDateRegistration() {
		return dateRegistration;
	}

	public void setDateRegistration(LocalDate dateRegistration) {
		this.dateRegistration = dateRegistration;
	}

	public String getNbOfStreet() {
		return nbOfStreet;
	}

	public void setNbOfStreet(String nbOfStreet) {
		this.nbOfStreet = nbOfStreet;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
}
