package fr.eql.ai110.project3.Hybribody.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("mother_plate_screening")
public class MotherPlatePicking extends Experiment {

	private static final long serialVersionUID = 1L;

	@ManyToMany
	@JoinTable(name = "T_motherplate_round_Associations",
			joinColumns = @JoinColumn(name = "idexperiment"),
			inverseJoinColumns = @JoinColumn(name = "idscreeninground"))
	private List<ScreeningRound> pickedRounds;
	@ManyToMany
	@JoinTable(name = "T_motherplate_xpcontrol_Associations",
			joinColumns = @JoinColumn(name = "idexperiment"),
			inverseJoinColumns = @JoinColumn(name = "idconsumable"))
	private List<ExperimentalControl> controls;
	@OneToMany(mappedBy = "motherPlatePicking", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Antibody> createdAntibodies;
	@Column(name="nb_plate")
	private String nbPlate;
	@ManyToOne
	@JoinColumn(name="id_material")
	private Plate plateUsed;
	
	public MotherPlatePicking() {
		super();
	}
	
	public MotherPlatePicking(List<ScreeningRound> pickedRounds, List<ExperimentalControl> controls,
			List<Antibody> createdAntibodies, String nbPlate, Plate plateUsed) {
		super();
		this.pickedRounds = pickedRounds;
		this.controls = controls;
		this.createdAntibodies = createdAntibodies;
		this.nbPlate = nbPlate;
		this.plateUsed = plateUsed;
	}

	public List<ScreeningRound> getPickedRounds() {
		return pickedRounds;
	}

	public void setPickedRounds(List<ScreeningRound> pickedRounds) {
		this.pickedRounds = pickedRounds;
	}

	public List<ExperimentalControl> getControls() {
		return controls;
	}

	public void setControls(List<ExperimentalControl> controls) {
		this.controls = controls;
	}

	public List<Antibody> getCreatedAntibodies() {
		return createdAntibodies;
	}

	public void setCreatedAntibodies(List<Antibody> createdAntibodies) {
		this.createdAntibodies = createdAntibodies;
	}

	public String getNbPlate() {
		return nbPlate;
	}

	public void setNbPlate(String nbPlate) {
		this.nbPlate = nbPlate;
	}

	public Plate getPlateUsed() {
		return plateUsed;
	}

	public void setPlateUsed(Plate plateUsed) {
		this.plateUsed = plateUsed;
	}
	
}
