package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Antibody;
import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.model.ScientificValue;
import fr.eql.ai110.project3.Hybribody.service.AntibodyIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ExperimentalControlIBusiness;
import fr.eql.ai110.project3.Hybribody.service.MotherPlatePickingIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ScreeningIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ScreeningRoundIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ValueIBusiness;

@Controller(value = "mbXPDetails")
@Scope(value = "session")
public class ExperimentDetailsManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Employee employee;
	private Project project;
	private Experiment experiment;
	private Screening screening;
	private MotherPlatePicking motherPlatePicking;

	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private ExperimentIBusiness experimentBusiness;
	@Autowired
	private ExperimentalControlIBusiness experimentalControlBusiness;
	@Autowired
	private ScreeningIBusiness screeningBusiness;
	@Autowired
	private ScreeningRoundIBusiness screeningRoundBusiness;
	@Autowired
	private MotherPlatePickingIBusiness motherPlatePickingBusiness;
	@Autowired
	private AntibodyIBusiness antibodyBusiness;
	@Autowired
	private ValueIBusiness valueBusiness;
	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
	}
	
	public String loadExperimentDetails(Integer id) {
		String redirect = "";
		experiment = experimentBusiness.getById(id);
		project = projectBusiness.getByExperiment(experiment);
		if(experimentBusiness.getCategory(id).equals("screening")) {
			screening = screeningBusiness.getById(id);
			if(screening != null) {
				redirect = "/screeningDetails.xhtml?faces-redirect=true";
			} else {
				redirect = "/projectDetails.xhtml?faces-redirect=false";
			}
		} else if (experimentBusiness.getCategory(id).equals("mother_plate_screening")) {
			motherPlatePicking = motherPlatePickingBusiness.getById(id);
			if(motherPlatePicking != null) {
				redirect = "/motherPlatePickingDetails.xhtml?faces-redirect=true";
			} else {
				redirect = "/projectDetails.xhtml?faces-redirect=false";
			}
		}
		return redirect;
	}
	
	public ScientificValue displayMeanInputByRound(Integer idRound) {
		ScientificValue value = valueBusiness.getByScreeningRoundIdAndValueCategoryName(idRound, "Mean Input");
		return value;
	}
	
	public ScientificValue displayMeanOutputByRound(Integer idRound) {
		ScientificValue value = valueBusiness.getByScreeningRoundIdAndValueCategoryName(idRound, "Mean Output");
		return value;
	}
	
	public ScientificValue displayRatioByRound(Integer idRound) {
		ScientificValue value = valueBusiness.getByScreeningRoundIdAndValueCategoryName(idRound, "Ratio");
		return value;
	}
	
	public ScientificValue displayEnrichmentByRound(Integer idRound) {
		ScientificValue value = valueBusiness.getByScreeningRoundIdAndValueCategoryName(idRound, "Enrichment");
		return value;
	}
	
	public List<ExperimentalControl> displayControlsUsed() {
		List<ExperimentalControl> controlsUsed = experimentalControlBusiness.getControls(motherPlatePicking);
		return controlsUsed;
	}
	
	public List<Antibody> displayAntibodiesPicked() {
		List<Antibody> antibodiesPicked = antibodyBusiness.getAntibodiesPicked(motherPlatePicking);
		return antibodiesPicked;
	}
	
	public List<ScreeningRound> displayRoundsPicked() {
		List<ScreeningRound> roundsPicked = screeningRoundBusiness.getRoundsPicked(motherPlatePicking);
		return roundsPicked;
	}
	
	public List<ScreeningRound> displayValidatedAndSuccessRound() {
		List<ScreeningRound> roundsValidatedAndSuccess = screeningRoundBusiness.getRoundsValidatedAndSuccess(screening);
		return roundsValidatedAndSuccess;
	}
	
	public List<ScreeningRound> displayRoundInProgress(){
		List<ScreeningRound> roundsInProgress = screeningRoundBusiness.getRoundsInProgress(screening);
		return roundsInProgress;
	}
	
	public List<ScreeningRound> displayRoundValidated(){
		List<ScreeningRound> roundsValidated = screeningRoundBusiness.getRoundsValidated(screening);
		return roundsValidated;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Screening getScreening() {
		return screening;
	}

	public void setScreening(Screening screening) {
		this.screening = screening;
	}

	public Experiment getExperiment() {
		return experiment;
	}

	public void setExperiment(Experiment experiment) {
		this.experiment = experiment;
	}

	public MotherPlatePicking getMotherPlatePicking() {
		return motherPlatePicking;
	}

	public void setMotherPlatePicking(MotherPlatePicking motherPlatePicking) {
		this.motherPlatePicking = motherPlatePicking;
	}
	
}
