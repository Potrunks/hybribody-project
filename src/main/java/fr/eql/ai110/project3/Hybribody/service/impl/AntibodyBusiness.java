package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Antibody;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.repository.AntibodyIDAO;
import fr.eql.ai110.project3.Hybribody.service.AntibodyIBusiness;

@Service
public class AntibodyBusiness implements AntibodyIBusiness {

	@Autowired
	private AntibodyIDAO antibodyDAO;
	
	@Override
	public List<Antibody> getAntibodiesPicked(MotherPlatePicking motherPlatePicking) {
		return antibodyDAO.findByMotherPlatePicking(motherPlatePicking);
	}

}
