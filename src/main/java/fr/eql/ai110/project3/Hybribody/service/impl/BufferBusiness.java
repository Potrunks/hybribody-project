package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Buffer;
import fr.eql.ai110.project3.Hybribody.repository.BufferIDAO;
import fr.eql.ai110.project3.Hybribody.service.BufferIBusiness;

@Service
public class BufferBusiness implements BufferIBusiness {

	@Autowired
	private BufferIDAO bufferDAO;

	@Override
	public List<Buffer> getAllByScreeningType(String screeningType) {
		List<Buffer> buffers = new ArrayList<Buffer>();
		if (screeningType.equals("Phage Display")) {
			buffers = bufferDAO.findByApplicationIs("Liquid Bacterial Culture");
		}
		return buffers;
	}

	@Override
	public Buffer getByIdBuffer(Integer idBuffer) {
		return bufferDAO.findById(idBuffer);
	}

	@Override
	public Buffer getByNameAndPH(String bufferName, Float pH) {
		return bufferDAO.findByNameAndPH(bufferName, pH);
	}

}
