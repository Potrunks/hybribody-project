package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;

public interface ScreeningRoundIDAO extends CrudRepository<ScreeningRound, Long> {

	List<ScreeningRound> findByScreeningAndDateValidationIsNotNullAndValidTrueOrderByNbAsc(Screening screening);
	List<ScreeningRound> findByScreeningAndDateValidationIsNullOrderByNbAsc(Screening screening);
	List<ScreeningRound> findByScreeningAndDateValidationIsNotNullOrderByNbAsc(Screening screening);
	List<ScreeningRound> findByMotherPlatePickingsOrderByNbAsc(MotherPlatePicking motherPlatePicking);
	ScreeningRound findById(Integer idScreeningRound);
	long countByDateValidationIsNull();
}
