package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.ScientificValue;

public interface ValueIDAO extends CrudRepository<ScientificValue, Long> {

	@Query(value="SELECT v.* "
			+ "FROM scientific_value v "
			+ "inner join screeninground scro on v.idscreeninground = scro.idscreeninground "
			+ "inner join valuecategory vcat on vcat.idvaluecategory = v.idvaluecategory "
			+ "where scro.idscreeninground=:idRoundParam "
			+ "and vcat.name=:valueCatNameParam", nativeQuery = true)
	ScientificValue findByScreeningRoundIdAndValueCategoryName(@Param("idRoundParam") Integer idRound, @Param("valueCatNameParam") String valueCatName);
	@Query(value="SELECT v.* "
			+ "FROM scientific_value v "
			+ "inner join valuecategory vc on vc.idvaluecategory = v.idvaluecategory "
			+ "inner join consumable c on c.idvalue = v.idvalue "
			+ "where vc.name = 'pH' "
			+ "and c.name = :nameBuffer "
			+ "group by v.idvalue", nativeQuery = true)
	List<ScientificValue> findPHByNameBufferGroupByPH(@Param("nameBuffer") String nameBuffer);
	@Query(value="SELECT v.* "
			+ "FROM scientific_value v "
			+ "inner join valuecategory vc on vc.idvaluecategory = v.idvaluecategory "
			+ "where vc.name = 'Temperature'", nativeQuery = true)
	List<ScientificValue> findAllTemperature();
	ScientificValue findById(Integer idValue);
}
