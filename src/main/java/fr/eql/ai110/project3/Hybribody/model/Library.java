package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="library")
public class Library implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_library")
	private Integer id;
	@Column(name = "name")
	private String name;
	@ManyToOne
	@JoinColumn(name="idvalue")
	private ScientificValue complexity;
	@ManyToOne
	@JoinColumn(name="idlibrarycategory")
	private LibraryCategory category;
	@OneToMany(mappedBy = "library", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Screening> screenings;
	// Cloning
	@OneToMany(mappedBy = "library", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<LibraryPreparation> preparations;

	public Library() {
		super();
	}

	public Library(Integer id, String name, ScientificValue complexity, LibraryCategory category,
			List<Screening> screenings, List<LibraryPreparation> preparations) {
		super();
		this.id = id;
		this.name = name;
		this.complexity = complexity;
		this.category = category;
		this.screenings = screenings;
		this.preparations = preparations;
	}

	public LibraryCategory getCategory() {
		return category;
	}

	public void setCategory(LibraryCategory category) {
		this.category = category;
	}

	public List<Screening> getScreenings() {
		return screenings;
	}

	public void setScreenings(List<Screening> screenings) {
		this.screenings = screenings;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ScientificValue getComplexity() {
		return complexity;
	}

	public void setComplexity(ScientificValue complexity) {
		this.complexity = complexity;
	}

	public List<LibraryPreparation> getPreparations() {
		return preparations;
	}

	public void setPreparations(List<LibraryPreparation> preparations) {
		this.preparations = preparations;
	}

}
