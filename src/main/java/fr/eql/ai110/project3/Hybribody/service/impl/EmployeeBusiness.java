package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.repository.EmployeeIDAO;
import fr.eql.ai110.project3.Hybribody.service.EmployeeIBusiness;

@Service
public class EmployeeBusiness implements EmployeeIBusiness {

	@Autowired
	private EmployeeIDAO employeeDAO;
	
	@Override
	public List<Employee> getAllOrderByDateArrivingAsc() {
		return employeeDAO.findByDateArrivingIsNotNullOrderByDateArrivingAsc();
	}

	@Override
	public Employee getByLogin(String mail) {
		return employeeDAO.findByMail(mail);
	}

}
