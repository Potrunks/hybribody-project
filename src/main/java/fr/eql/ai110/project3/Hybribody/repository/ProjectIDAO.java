package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;

public interface ProjectIDAO extends CrudRepository<Project, Long> {

	List<Project> findByDateClosingIsNullAndDateCancelationIsNullOrderByDateCreationDesc();
	Project findByIdproject(Integer idProject);
	Project findByExperiments(Experiment experiment);
	@Query(value="select p.* "
			+ "from project p "
			+ "inner join t_experiment_project_associations xpp on p.idproject = xpp.idproject "
			+ "inner join experiment xp on xpp.idexperiment = xp.idexperiment "
			+ "where p.dateclosing is null "
			+ "and p.datecancelation is null "
			+ "and xp.category = 'mother_plate_screening' "
			+ "and xp.datevalidation is null "
			+ "GROUP BY p.idproject "
			+ "order by p.datecreation desc", nativeQuery = true)
	List<Project> findAllInProgressAndMotherPlateInProgress();
	@Query(value = "SELECT p.* "
			+ "FROM project p "
			+ "INNER JOIN t_experiment_project_associations xpp ON p.idproject = xpp.idproject "
			+ "INNER JOIN experiment xp ON xpp.idexperiment = xp.idexperiment "
			+ "INNER JOIN screeninground scr ON xp.idexperiment = scr.idexperiment "
			+ "WHERE p.dateclosing IS NULL "
			+ "AND p.datecancelation IS NULL "
			+ "AND scr.datevalidation IS NULL "
			+ "GROUP BY p.idproject "
			+ "ORDER BY p.datecreation DESC", nativeQuery = true)
	List<Project> findAllInProgressAndScreeningRoundInProgress();
	@Query(value="select p.* "
			+ "from project p "
			+ "inner join customer c on p.idcustomer = c.idcustomer "
			+ "left join t_experiment_project_associations xpp on p.idproject = xpp.idproject "
			+ "left join experiment xp on xp.idexperiment = xpp.idexperiment "
			+ "left join t_subscription_project_associations subp on subp.idproject = p.idproject "
			+ "left join subscription sub on sub.idsubscription = subp.idsubscription "
			+ "where p.name like :projectName "
			+ "and p.datecreation is not null "
			+ "and p.dateclosing is null "
			+ "and p.datecancelation is null "
			+ "and p.success = 0 "
			+ "and c.surname like :customerSurname "
			+ "and c.name like :customerName "
			+ "and (xp.category like :xpCategory or :xpCategory = '%%') "
			+ "and (sub.idsubscription = :idSubscription or :idSubscription = 0) "
			+ "group by p.idproject "
			+ "ORDER BY p.datecreation DESC", nativeQuery = true)
	List<Project> findInProgressByMultiCriteria(@Param("projectName") String projectName,@Param("customerSurname") String customerSurname,
			@Param("customerName") String customerName,@Param("xpCategory") String xpCategory,
			@Param("idSubscription") Integer subscriptionName);
	@Query(value="select p.* "
			+ "from project p "
			+ "inner join customer c on p.idcustomer = c.idcustomer "
			+ "left join t_experiment_project_associations xpp on p.idproject = xpp.idproject "
			+ "left join experiment xp on xp.idexperiment = xpp.idexperiment "
			+ "left join t_subscription_project_associations subp on subp.idproject = p.idproject "
			+ "left join subscription sub on sub.idsubscription = subp.idsubscription "
			+ "where p.name like :projectName "
			+ "and p.datecreation is not null "
			+ "and p.dateclosing is not null "
			+ "and p.datecancelation is null "
			+ "and p.success = 1 "
			+ "and c.surname like :customerSurname "
			+ "and c.name like :customerName "
			+ "and (xp.category like :xpCategory or :xpCategory = '%%') "
			+ "and (sub.idsubscription = :idSubscription or :idSubscription = 0) "
			+ "group by p.idproject "
			+ "ORDER BY p.datecreation DESC", nativeQuery = true)
	List<Project> findSuccessfullyValidateByMultiCriteria(@Param("projectName") String projectName,@Param("customerSurname") String customerSurname,
			@Param("customerName") String customerName,@Param("xpCategory") String xpCategory,
			@Param("idSubscription") Integer subscriptionName);
	@Query(value="select p.* "
			+ "from project p "
			+ "inner join customer c on p.idcustomer = c.idcustomer "
			+ "left join t_experiment_project_associations xpp on p.idproject = xpp.idproject "
			+ "left join experiment xp on xp.idexperiment = xpp.idexperiment "
			+ "left join t_subscription_project_associations subp on subp.idproject = p.idproject "
			+ "left join subscription sub on sub.idsubscription = subp.idsubscription "
			+ "where p.name like :projectName "
			+ "and p.datecreation is not null "
			+ "and p.dateclosing is not null "
			+ "and p.datecancelation is null "
			+ "and p.success = 0 "
			+ "and c.surname like :customerSurname "
			+ "and c.name like :customerName "
			+ "and (xp.category like :xpCategory or :xpCategory = '%%') "
			+ "and (sub.idsubscription = :idSubscription or :idSubscription = 0) "
			+ "group by p.idproject "
			+ "ORDER BY p.datecreation DESC", nativeQuery = true)
	List<Project> findUnsuccessfullyValidateByMultiCriteria(@Param("projectName") String projectName,@Param("customerSurname") String customerSurname,
			@Param("customerName") String customerName,@Param("xpCategory") String xpCategory,
			@Param("idSubscription") Integer subscriptionName);
	@Query(value="select p.* "
			+ "from project p "
			+ "inner join customer c on p.idcustomer = c.idcustomer "
			+ "left join t_experiment_project_associations xpp on p.idproject = xpp.idproject "
			+ "left join experiment xp on xp.idexperiment = xpp.idexperiment "
			+ "left join t_subscription_project_associations subp on subp.idproject = p.idproject "
			+ "left join subscription sub on sub.idsubscription = subp.idsubscription "
			+ "where p.name like :projectName "
			+ "and p.datecreation is not null "
			+ "and p.datecancelation is not null "
			+ "and c.surname like :customerSurname "
			+ "and c.name like :customerName "
			+ "and (xp.category like :xpCategory or :xpCategory = '%%') "
			+ "and (sub.idsubscription = :idSubscription or :idSubscription = 0) "
			+ "group by p.idproject "
			+ "ORDER BY p.datecreation DESC", nativeQuery = true)
	List<Project> findCancelByMultiCriteria(@Param("projectName") String projectName,@Param("customerSurname") String customerSurname,
			@Param("customerName") String customerName,@Param("xpCategory") String xpCategory,
			@Param("idSubscription") Integer subscriptionName);
	@Query(value="select p.* "
			+ "from project p "
			+ "inner join customer c on p.idcustomer = c.idcustomer "
			+ "left join t_experiment_project_associations xpp on p.idproject = xpp.idproject "
			+ "left join experiment xp on xp.idexperiment = xpp.idexperiment "
			+ "left join t_subscription_project_associations subp on subp.idproject = p.idproject "
			+ "left join subscription sub on sub.idsubscription = subp.idsubscription "
			+ "where p.name like :projectName "
			+ "and c.surname like :customerSurname "
			+ "and c.name like :customerName "
			+ "and (xp.category like :xpCategory or :xpCategory = '%%') "
			+ "and (sub.idsubscription = :idSubscription or :idSubscription = 0) "
			+ "group by p.idproject "
			+ "order by p.datecreation desc", nativeQuery = true)
	List<Project> findAllByMultiCriteria(@Param("projectName") String projectName,@Param("customerSurname") String customerSurname,
			@Param("customerName") String customerName,@Param("xpCategory") String xpCategory,
			@Param("idSubscription") Integer subscriptionName);
	@Query(value="SELECT p.* "
			+ "FROM project p "
			+ "INNER JOIN t_experiment_project_associations xpp ON xpp.idproject = p.idproject "
			+ "INNER JOIN experiment xp ON xpp.idexperiment = xp.idexperiment "
			+ "INNER JOIN screeninground scrro ON scrro.idexperiment = xp.idexperiment "
			+ "WHERE xp.category = 'screening' "
			+ "and p.dateclosing is null "
			+ "AND xp.datecreation is not null "
			+ "and xp.datevalidation is not null "
			+ "and xp.valid = 1 "
			+ "AND scrro.datecreation is not null "
			+ "and scrro.datevalidation is not null "
			+ "and scrro.valid = 1 "
			+ "GROUP BY p.idproject", nativeQuery = true)
	List<Project> findByAvailableScreeningAndAvailableRound();
	@Query(value="SELECT COUNT(*) "
			+ "FROM project p "
			+ "INNER JOIN t_experiment_project_associations xpp ON xpp.idproject = p.idproject "
			+ "INNER JOIN experiment xp ON xpp.idexperiment = xp.idexperiment "
			+ "INNER JOIN screeninground scrro ON scrro.idexperiment = xp.idexperiment "
			+ "WHERE xp.category = 'screening' "
			+ "and p.dateclosing is null "
			+ "AND xp.datecreation is not null "
			+ "and xp.datevalidation is not null "
			+ "and xp.valid = 1 "
			+ "AND scrro.datecreation is not null "
			+ "and scrro.datevalidation is not null "
			+ "and scrro.valid = 1 "
			+ "GROUP BY p.idproject "
			+ "limit 1", nativeQuery = true)
	Long countByAvailableScreeningAndAvailableRound();
	@Query(value="SELECT COUNT(*) "
			+ "FROM project p "
			+ "INNER JOIN t_experiment_project_associations xpp ON xpp.idproject = p.idproject "
			+ "INNER JOIN experiment xp ON xpp.idexperiment = xp.idexperiment "
			+ "INNER JOIN screeninground scrro ON scrro.idexperiment = xp.idexperiment "
			+ "WHERE xp.category = 'screening' "
			+ "and p.dateclosing is null "
			+ "AND xp.datecreation is not null "
			+ "and xp.datevalidation is not null "
			+ "and xp.valid = 1 "
			+ "AND scrro.datecreation is not null "
			+ "and scrro.datevalidation is not null "
			+ "and scrro.valid = 1 "
			+ "and p.idproject = :idProject", nativeQuery = true)
	Long countByAvailableScreeningAndAvailableRoundAndIdProject(@Param("idProject") Integer idProject);
	Long countByDateCreationIsNotNull();
	Long countByDateCreationIsNotNullAndDateClosingIsNullAndDateCancelationIsNull();
	Long countByDateCreationIsNotNullAndDateClosingIsNotNullAndDateCancelationIsNullAndSuccessTrue();
	Long countByDateCreationIsNotNullAndDateClosingIsNotNullAndDateCancelationIsNullAndSuccessFalse();
	Long countByDateCreationIsNotNullAndDateClosingIsNullAndDateCancelationIsNotNull();
	List<Project> findFirst3ByNameLikeAndIdprojectNotOrderByDateCreationDesc(String name, Integer idproject);
}
