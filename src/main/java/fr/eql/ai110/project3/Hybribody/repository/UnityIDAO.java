package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Unity;
import fr.eql.ai110.project3.Hybribody.model.ValueCategory;

public interface UnityIDAO extends CrudRepository<Unity, Long> {

	public List<Unity> findByCategories(ValueCategory category);
}
