package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "library_preparation_category")
public class LibraryPreparationCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_library_preparation_category")
	private Integer id;
	@Column(name = "name")
	private String name;
	@OneToMany(mappedBy = "category", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<LibraryPreparation> preparations;
	
	public LibraryPreparationCategory() {
		super();
	}

	public LibraryPreparationCategory(Integer id, String name, List<LibraryPreparation> preparations) {
		super();
		this.id = id;
		this.name = name;
		this.preparations = preparations;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<LibraryPreparation> getPreparations() {
		return preparations;
	}

	public void setPreparations(List<LibraryPreparation> preparations) {
		this.preparations = preparations;
	}

}
