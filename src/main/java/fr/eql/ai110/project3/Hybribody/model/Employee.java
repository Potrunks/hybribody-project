package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idemployee")
	private Integer id;
	@Column(name = "surname")
	private String surname;
	@Column(name = "name")
	private String name;
	@Column(name = "mail")
	private String mail;
	@Column(name = "datecreation")
	private LocalDate dateCreation;
	@Column(name = "datedeparture")
	private LocalDate dateDeparture;
	@Column(name = "initial")
	private String initial;
	@Column(name = "datearriving")
	private LocalDate dateArriving;
	@Column(name = "admin")
	private boolean isAdmin;
	@Column(name = "datemailregistration")
	private LocalDate dateMailRegistration;
	@Column(name = "password")
	private String password;
	@Column(name = "salt")
	private String salt;
	@ManyToOne
	@JoinColumn(name = "idtitle")
	private Title title;
	@OneToMany(mappedBy = "employee", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Task> tasks;
	@OneToMany(mappedBy = "employee", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Consumable> consumables;
	@OneToMany(mappedBy = "employee", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Experiment> experiments;
	
	public Employee() {
		super();
	}

	public Employee(Integer id, String surname, String name, String mail, LocalDate dateCreation,
			LocalDate dateDeparture, String initial, LocalDate dateArriving, boolean isAdmin,
			LocalDate dateMailRegistration, String password, String salt, Title title, List<Task> tasks,
			List<Consumable> consumables, List<Experiment> experiments) {
		super();
		this.id = id;
		this.surname = surname;
		this.name = name;
		this.mail = mail;
		this.dateCreation = dateCreation;
		this.dateDeparture = dateDeparture;
		this.initial = initial;
		this.dateArriving = dateArriving;
		this.isAdmin = isAdmin;
		this.dateMailRegistration = dateMailRegistration;
		this.password = password;
		this.salt = salt;
		this.title = title;
		this.tasks = tasks;
		this.consumables = consumables;
		this.experiments = experiments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public LocalDate getDateDeparture() {
		return dateDeparture;
	}

	public void setDateDeparture(LocalDate dateDeparture) {
		this.dateDeparture = dateDeparture;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public LocalDate getDateArriving() {
		return dateArriving;
	}

	public void setDateArriving(LocalDate dateArriving) {
		this.dateArriving = dateArriving;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public LocalDate getDateMailRegistration() {
		return dateMailRegistration;
	}

	public void setDateMailRegistration(LocalDate dateMailRegistration) {
		this.dateMailRegistration = dateMailRegistration;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<Consumable> getConsumables() {
		return consumables;
	}

	public void setConsumables(List<Consumable> consumables) {
		this.consumables = consumables;
	}

	public List<Experiment> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<Experiment> experiments) {
		this.experiments = experiments;
	}
	
}
