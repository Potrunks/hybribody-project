package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;

public interface SubscriptionIDAO extends CrudRepository<Subscription, Long> {
	
	List<Subscription> findByProjects(Project project);
	List<Subscription> findByIdSubscriptionParentIsNull();
	List<Subscription> findByIdSubscriptionParent(Integer idSubscriptionParent);
	Subscription findByIdsubscription(Integer idsubscription);
	@Query(value="select s.idsubscription "
			+ "from subscription s "
			+ "inner join t_subscription_project_associations sp on sp.idsubscription = s.idsubscription "
			+ "inner join project p on p.idproject = sp.idproject "
			+ "where p.idproject = :idProject "
			+ "and s.idsubscriptionparent = :idParentSubscription", nativeQuery = true)
	List<Integer> findIdSubscriptionsByIdProjectAndIdParentSubscription(@Param("idProject") Integer idProject,@Param("idParentSubscription") Integer idParentSubscription);
}
