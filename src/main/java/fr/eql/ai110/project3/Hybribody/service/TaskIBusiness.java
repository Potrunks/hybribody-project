package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Task;

public interface TaskIBusiness {

	public List<Task> getAllOrderByDateCreationDesc();
}
