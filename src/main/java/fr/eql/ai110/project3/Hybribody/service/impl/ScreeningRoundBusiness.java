package fr.eql.ai110.project3.Hybribody.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;
import fr.eql.ai110.project3.Hybribody.model.Task;
import fr.eql.ai110.project3.Hybribody.repository.ProjectIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningIDAO;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningRoundIDAO;
import fr.eql.ai110.project3.Hybribody.repository.TaskIDAO;
import fr.eql.ai110.project3.Hybribody.service.ScreeningRoundIBusiness;

@Service
public class ScreeningRoundBusiness implements ScreeningRoundIBusiness {

	@Autowired
	private ScreeningRoundIDAO screeningRoundDAO;
	@Autowired
	private ScreeningIDAO screeningDAO;
	@Autowired
	private TaskIDAO taskDAO;
	@Autowired
	private ProjectIDAO projectDAO;

	@Override
	public List<ScreeningRound> getRoundsValidatedAndSuccess(Screening screening) {
		return screeningRoundDAO.findByScreeningAndDateValidationIsNotNullAndValidTrueOrderByNbAsc(screening);
	}

	@Override
	public List<ScreeningRound> getRoundsValidated(Screening screening) {
		return screeningRoundDAO.findByScreeningAndDateValidationIsNotNullOrderByNbAsc(screening);
	}

	@Override
	public List<ScreeningRound> getRoundsInProgress(Screening screening) {
		return screeningRoundDAO.findByScreeningAndDateValidationIsNullOrderByNbAsc(screening);
	}

	@Override
	public List<ScreeningRound> getRoundsPicked(MotherPlatePicking motherPlatePicking) {
		return screeningRoundDAO.findByMotherPlatePickingsOrderByNbAsc(motherPlatePicking);
	}

	@Override
	public ScreeningRound getById(Integer idScreeningRound) {
		return screeningRoundDAO.findById(idScreeningRound);
	}

	@Override
	public boolean verifyByScreeningIfPreviousRoundExist(Integer idScreening, Integer idRound) {
		boolean exist = false;
		ScreeningRound round = screeningRoundDAO.findById(idRound);
		if (round.getNb() != 1) {
			Screening screening = screeningDAO.findById(idScreening);
			for (ScreeningRound scrRo : screening.getRounds()) {
				if (scrRo.getNb() == round.getNb() - 1 && scrRo.isValid() == true) {
					exist = true;
					break;
				}
			}
		}
		return exist;
	}

	@Override
	public void validate(Integer idScreeningRound, String validationCause, boolean isSuccess) {
		ScreeningRound round = screeningRoundDAO.findById(idScreeningRound);
		round.setValidationCause(validationCause);
		round.setValid(isSuccess);
		round.setDateValidation(LocalDate.now());
		screeningRoundDAO.save(round);
		Task task = new Task();
		task.setDateCreation(LocalDateTime.now());
		task.setDateFormat(task.getDateCreation());
		task.setEmployee(round.getScreening().getEmployee());
		Project project = projectDAO.findByExperiments(round.getScreening());
		task.setTaskDone("Project " + project.getName() + " - " + round.getScreening().getName() + " - Round " + round.getNb() + " validated");
		taskDAO.save(task);
	}

	@Override
	public boolean verifyRoundInProgressExist() {
		boolean exist = false; 
		if(screeningRoundDAO.countByDateValidationIsNull() != 0) {
			exist = true;
		}
		return exist;
	}

	@Override
	public List<ScreeningRound> getByMultipleIdScreeningRound(List<Integer> idRounds) {
		List<ScreeningRound> rounds = new ArrayList<ScreeningRound>();
		for (Integer idRound : idRounds) {
			if(idRound != null) {
				rounds.add(screeningRoundDAO.findById(idRound));
			}
		}
		return rounds;
	}

}
