package fr.eql.ai110.project3.Hybribody.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.repository.ScreeningIDAO;
import fr.eql.ai110.project3.Hybribody.service.ScreeningIBusiness;

@Service
public class ScreeningBusiness implements ScreeningIBusiness {

	@Autowired
	private ScreeningIDAO screeningDAO;
	
	@Override
	public Screening getById(Integer idExperiment) {
		return screeningDAO.findById(idExperiment);
	}

	@Override
	public List<Screening> getAllInProgressWithRoundInProgressByProject(Integer idProject) {
		return screeningDAO.findInProgressByIdProjectWhereScreeningRoundInProgress(idProject);
	}

	@Override
	public List<Screening> getAllEligibleScreeningForMotherPlatePicking(Integer idProject) {
		return screeningDAO.findAvailableScreeningsWithAvailableRoundsForMotherPlatePicking(idProject);
	}

}
