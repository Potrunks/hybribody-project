package fr.eql.ai110.project3.Hybribody;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HybribodyApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(HybribodyApplication.class, args);
	}

}
