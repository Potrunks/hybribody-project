package fr.eql.ai110.project3.Hybribody.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("library_preparation")
public class LibraryPreparation extends Consumable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name="id_library_preparation_category")
	private LibraryPreparationCategory category;
	@ManyToOne
	@JoinColumn(name="id_library")
	private Library library;
	
	public LibraryPreparation() {
		super();
	}

	public LibraryPreparation(LibraryPreparationCategory category, Library library) {
		super();
		this.category = category;
		this.library = library;
	}

	public LibraryPreparationCategory getCategory() {
		return category;
	}

	public void setCategory(LibraryPreparationCategory category) {
		this.category = category;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

}
