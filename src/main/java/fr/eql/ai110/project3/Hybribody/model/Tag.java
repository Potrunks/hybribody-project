package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tag")
public class Tag implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idtag")
	private Integer id;
	@Column(name="name")
	private String name;
	@ManyToMany
	@JoinTable(name = "T_target_tag_Associations",
			joinColumns = @JoinColumn(name = "idtag"),
			inverseJoinColumns = @JoinColumn(name = "idconsumable"))
	private List<Target> targets;
	@ManyToOne
	@JoinColumn(name = "idlabel")
	private Label label;
	
	public Tag() {
		super();
	}

	public Tag(Integer id, String name, List<Target> targets, Label label) {
		super();
		this.id = id;
		this.name = name;
		this.targets = targets;
		this.label = label;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}
	
}
