package fr.eql.ai110.project3.Hybribody.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;

public interface ExperimentIDAO extends CrudRepository<Experiment, Long> {

	// Get the Most Recent Experiment In Progress
	@Query(value = "SELECT * "
			+ "FROM experiment e "
			+ "INNER JOIN t_experiment_project_associations ep ON e.idexperiment = ep.idexperiment "
			+ "INNER JOIN project p ON p.idproject = ep.idproject "
			+ "WHERE e.datecreation IS NOT NULL "
			+ "AND p.idproject=:idParam "
			+ "AND e.datevalidation IS NULL "
			+ "AND e.datecancelation IS NULL "
			+ "ORDER BY e.datecreation DESC "
			+ "LIMIT 1", nativeQuery = true)
	Experiment findByMostRecentDateCreation(@Param("idParam") Integer idProject);
	
	// Find last experiment validated
	@Query(value = "SELECT * "
			+ "FROM experiment e "
			+ "INNER JOIN t_experiment_project_associations ep ON e.idexperiment = ep.idexperiment "
			+ "INNER JOIN project p ON p.idproject = ep.idproject "
			+ "WHERE e.datecreation IS NOT NULL "
			+ "AND p.idproject=:idParam "
			+ "AND e.datevalidation IS NOT NULL "
			+ "AND e.datecancelation IS NULL "
			+ "ORDER BY e.datevalidation DESC "
			+ "LIMIT 1", nativeQuery = true)
	Experiment findByMostRecentDateValidation(@Param("idParam") Integer idProject);
	
	// Find all the experiment in progress in the project
	List<Experiment> findByProjectsAndDateValidationIsNullAndDateCancelationIsNullOrderByDateCreationDesc(Project project);
	
	// Find all the experiment validated
	List<Experiment> findByProjectsAndDateValidationIsNotNullAndDateCancelationIsNullOrderByDateValidationDesc(Project project);
	
	//Find the Experiment by ID
	Experiment findById(Integer id);
	
	// Find the Category of Experiment
	@Query(value="select category from experiment where idexperiment=:idXpParam", nativeQuery = true)
	String findCategoryById(@Param("idXpParam") Integer idExperiment);
	@Query(value="SELECT * "
			+ "FROM experiment "
			+ "where category = :category "
			+ "order by idexperiment desc "
			+ "limit 1", nativeQuery = true)
	Experiment findLastCreatedByCategory(@Param("category") String category);
}
