package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Screening;

public interface ScreeningIBusiness {

	Screening getById(Integer idExperiment);
	List<Screening> getAllInProgressWithRoundInProgressByProject(Integer idProject);
	List<Screening> getAllEligibleScreeningForMotherPlatePicking(Integer idProject);
}
