package fr.eql.ai110.project3.Hybribody.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.project3.Hybribody.model.Customer;
import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;
import fr.eql.ai110.project3.Hybribody.service.CustomerIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ExperimentIBusiness;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;
import fr.eql.ai110.project3.Hybribody.service.SubscriptionIBusiness;

@Controller(value = "mbProject")
@Scope(value = "request")
public class ProjectManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private CustomerIBusiness customerBusiness;
	@Autowired
	private SubscriptionIBusiness subscriptionBusiness;
	@Autowired
	private ProjectIBusiness projectBusiness;
	@Autowired
	private ExperimentIBusiness experimentBusiness;

	private Employee employee;

	private String newProjectName;
	private Integer customerSelected;
	private String newProjectComment;
	private String newProjectCancelCause;
	private List<Integer> selectedSubscriptions1;
	private List<Integer> selectedSubscriptions2;
	private List<Integer> selectedSubscriptions3;
	private List<Integer> selectedSubscriptions4;
	private List<Integer> selectedSubscriptions5;
	private List<Integer> selectedSubscriptions6;
	private boolean selectedSuccess;
	private String newClosingComment;

	private List<Customer> customers;
	private List<Subscription> parentSubscriptions;
	private Subscription parentSubscription1;
	private Subscription parentSubscription2;
	private Subscription parentSubscription3;
	private Subscription parentSubscription4;
	private Subscription parentSubscription5;
	private Subscription parentSubscription6;
	private List<Subscription> childSubscriptions1;
	private List<Subscription> childSubscriptions2;
	private List<Subscription> childSubscriptions3;
	private List<Subscription> childSubscriptions4;
	private List<Subscription> childSubscriptions5;
	private List<Subscription> childSubscriptions6;
	private Project projectToModify;
	private Project projectEdited;
	private Project projectToCancel;
	private Project projectToClose;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		employee = (Employee) session.getAttribute("employee");
		projectToModify = (Project) session.getAttribute("projectToModify");
		projectToCancel = (Project) session.getAttribute("projectToCancel");
		customers = customerBusiness.getAll();
		parentSubscriptions = subscriptionBusiness.getAllParentSubscription();
		parentSubscription1 = parentSubscriptions.get(0);
		parentSubscription2 = parentSubscriptions.get(1);
		parentSubscription3 = parentSubscriptions.get(2);
		parentSubscription4 = parentSubscriptions.get(3);
		parentSubscription5 = parentSubscriptions.get(4);
		parentSubscription6 = parentSubscriptions.get(5);
		childSubscriptions1 = subscriptionBusiness.getAllSubSubscription(parentSubscription1.getIdsubscription());
		childSubscriptions2 = subscriptionBusiness.getAllSubSubscription(parentSubscription2.getIdsubscription());
		childSubscriptions3 = subscriptionBusiness.getAllSubSubscription(parentSubscription3.getIdsubscription());
		childSubscriptions4 = subscriptionBusiness.getAllSubSubscription(parentSubscription4.getIdsubscription());
		childSubscriptions5 = subscriptionBusiness.getAllSubSubscription(parentSubscription5.getIdsubscription());
		childSubscriptions6 = subscriptionBusiness.getAllSubSubscription(parentSubscription6.getIdsubscription());
	}
	
	public String displayNewProjectMode() {
		String redirect = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		projectToModify = null;
		session.setAttribute("projectToModify", projectToModify);
		redirect = "/projectCreator.xhtml";
		return redirect;
	}

	public String editNewProject() {
		String redirect = null;
		Customer customer = customerBusiness.getById(customerSelected);
		List<Integer> allSelectedSubscriptions = new ArrayList<Integer>();
		allSelectedSubscriptions.addAll(selectedSubscriptions1);
		allSelectedSubscriptions.addAll(selectedSubscriptions2);
		allSelectedSubscriptions.addAll(selectedSubscriptions3);
		allSelectedSubscriptions.addAll(selectedSubscriptions4);
		allSelectedSubscriptions.addAll(selectedSubscriptions5);
		allSelectedSubscriptions.addAll(selectedSubscriptions6);
		if (newProjectName == "" || customer == null) {
			if (newProjectName == "") {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("Project name is required");
				fm.setDetail("Project name is required");
				FacesContext.getCurrentInstance().addMessage("new-project-form:project-error", fm);
			}
			if (customer == null) {
				FacesMessage fm = new FacesMessage();
				fm.setSeverity(FacesMessage.SEVERITY_WARN);
				fm.setSummary("A customer is required");
				fm.setDetail("A customer is required");
				FacesContext.getCurrentInstance().addMessage("new-project-form:customer-error", fm);
			}
			redirect = "/projectCreator.xhtml?faces-redirect=false";
		} else {
			if(projectToModify != null) {
				projectEdited = projectBusiness.modify(projectToModify, newProjectName, newProjectComment, allSelectedSubscriptions, customer, employee);
				FacesContext facesContext = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
				projectToModify = null;
				session.setAttribute("projectToModify", projectToModify);
				session.setAttribute("projectEdited", projectEdited);
			} else {
				projectEdited = projectBusiness.create(newProjectName, newProjectComment, allSelectedSubscriptions, customer, employee);
				FacesContext facesContext = FacesContext.getCurrentInstance();
				HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
				session.setAttribute("projectEdited", projectEdited);
			}
			redirect = "/projectCreatorSuccess.xhtml?faces-redirect=true";
		}
		return redirect;
	}

	public String displayModifyProjectMode(Integer idProject) {
		String redirect = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		projectToModify = projectBusiness.getByIdProject(idProject);
		newProjectName = projectToModify.getName();
		customerSelected = projectToModify.getCustomer().getId();
		selectedSubscriptions1 = subscriptionBusiness.getIdSubscriptionsByIdProjectAndIdParentSubscription(
				projectToModify.getIdproject(), parentSubscription1.getIdsubscription());
		selectedSubscriptions2 = subscriptionBusiness.getIdSubscriptionsByIdProjectAndIdParentSubscription(
				projectToModify.getIdproject(), parentSubscription2.getIdsubscription());
		selectedSubscriptions3 = subscriptionBusiness.getIdSubscriptionsByIdProjectAndIdParentSubscription(
				projectToModify.getIdproject(), parentSubscription3.getIdsubscription());
		selectedSubscriptions4 = subscriptionBusiness.getIdSubscriptionsByIdProjectAndIdParentSubscription(
				projectToModify.getIdproject(), parentSubscription4.getIdsubscription());
		selectedSubscriptions5 = subscriptionBusiness.getIdSubscriptionsByIdProjectAndIdParentSubscription(
				projectToModify.getIdproject(), parentSubscription5.getIdsubscription());
		selectedSubscriptions6 = subscriptionBusiness.getIdSubscriptionsByIdProjectAndIdParentSubscription(
				projectToModify.getIdproject(), parentSubscription6.getIdsubscription());
		newProjectComment = projectToModify.getComment();
		session.setAttribute("projectToModify", projectToModify);
		redirect = "/projectCreator.xhtml";
		return redirect;
	}
	
	public String displayCancelProjectMode(Integer idProject) {
		String redirect = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		projectToCancel = projectBusiness.getByIdProject(idProject);
		newProjectName = projectToCancel.getName();
		session.setAttribute("projectToCancel", projectToCancel);
		redirect = "/cancelProjectForm.xhtml";
		return redirect;
	}
	
	public String cancelProject() {
		String redirect = null;
		if(newProjectCancelCause == "") {
			FacesMessage fm = new FacesMessage();
			fm.setSeverity(FacesMessage.SEVERITY_WARN);
			fm.setSummary("Cancelation cause is required");
			fm.setDetail("Cancelation cause is required");
			FacesContext.getCurrentInstance().addMessage("cancel-project-form:cause-error", fm);
			redirect = "/cancelProjectForm.xhtml?faces-redirect=false";
		} else {
			experimentBusiness.cancelAllExperimentsInProgressInProject(projectToCancel);
			projectEdited = projectBusiness.cancel(projectToCancel, newProjectCancelCause, employee);
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
			projectToCancel = null;
			session.setAttribute("projectToCancel", projectToCancel);
			session.setAttribute("projectEdited", projectEdited);
			redirect = "/projectCancelSuccess.xhtml?faces-redirect=true";
		}
		return redirect;
	}
	
	public void clear() {
		newProjectName = "";
		newProjectComment = "";
		customerSelected = null;
		selectedSubscriptions1 = null;
		selectedSubscriptions2 = null;
		selectedSubscriptions3 = null;
		selectedSubscriptions4 = null;
		selectedSubscriptions5 = null;
		selectedSubscriptions6 = null;
	}
	
	public String cancel() {
		String redirect = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		projectToModify = null;
		session.setAttribute("projectToModify", projectToModify);
		redirect = "/home.xhtml";
		return redirect;
	}
	
	public String displayClosingProjectMode(Integer idProject) {
		String redirect = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		projectToClose = projectBusiness.getByIdProject(idProject);
		newProjectName = projectToClose.getName();
		session.setAttribute("projectToClose", projectToClose);
		redirect = "/closeProjectForm.xhtml";
		return redirect;
	}
	
	public String closeProject() {
		String redirect = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		projectToClose = (Project) session.getAttribute("projectToClose");
		projectEdited = projectBusiness.close(projectToClose, newClosingComment, selectedSuccess, employee);
		projectToClose = null;
		session.setAttribute("projectToClose", projectToClose);
		session.setAttribute("projectEdited", projectEdited);
		redirect = "/closeProjectSuccess.xhtml";
		return redirect;
	}
	
	public String reopenProject(Integer idProject) {
		String redirect = null;
		projectEdited = projectBusiness.getByIdProject(idProject);
		projectEdited = projectBusiness.reopen(projectEdited, employee);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		session.setAttribute("projectEdited", projectEdited);
		redirect = "/reopenProjectSuccess.xhtml";
		return redirect;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getNewProjectName() {
		return newProjectName;
	}

	public void setNewProjectName(String newProjectName) {
		this.newProjectName = newProjectName;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public List<Subscription> getParentSubscriptions() {
		return parentSubscriptions;
	}

	public void setParentSubscriptions(List<Subscription> parentSubscriptions) {
		this.parentSubscriptions = parentSubscriptions;
	}

	public String getNewProjectComment() {
		return newProjectComment;
	}

	public void setNewProjectComment(String newProjectComment) {
		this.newProjectComment = newProjectComment;
	}

	public Integer getCustomerSelected() {
		return customerSelected;
	}

	public void setCustomerSelected(Integer customerSelected) {
		this.customerSelected = customerSelected;
	}

	public Subscription getParentSubscription1() {
		return parentSubscription1;
	}

	public void setParentSubscription1(Subscription parentSubscription1) {
		this.parentSubscription1 = parentSubscription1;
	}

	public Subscription getParentSubscription2() {
		return parentSubscription2;
	}

	public void setParentSubscription2(Subscription parentSubscription2) {
		this.parentSubscription2 = parentSubscription2;
	}

	public Subscription getParentSubscription3() {
		return parentSubscription3;
	}

	public void setParentSubscription3(Subscription parentSubscription3) {
		this.parentSubscription3 = parentSubscription3;
	}

	public Subscription getParentSubscription4() {
		return parentSubscription4;
	}

	public void setParentSubscription4(Subscription parentSubscription4) {
		this.parentSubscription4 = parentSubscription4;
	}

	public Subscription getParentSubscription5() {
		return parentSubscription5;
	}

	public void setParentSubscription5(Subscription parentSubscription5) {
		this.parentSubscription5 = parentSubscription5;
	}

	public Subscription getParentSubscription6() {
		return parentSubscription6;
	}

	public void setParentSubscription6(Subscription parentSubscription6) {
		this.parentSubscription6 = parentSubscription6;
	}

	public List<Subscription> getChildSubscriptions1() {
		return childSubscriptions1;
	}

	public void setChildSubscriptions1(List<Subscription> childSubscriptions1) {
		this.childSubscriptions1 = childSubscriptions1;
	}

	public List<Subscription> getChildSubscriptions2() {
		return childSubscriptions2;
	}

	public void setChildSubscriptions2(List<Subscription> childSubscriptions2) {
		this.childSubscriptions2 = childSubscriptions2;
	}

	public List<Subscription> getChildSubscriptions3() {
		return childSubscriptions3;
	}

	public void setChildSubscriptions3(List<Subscription> childSubscriptions3) {
		this.childSubscriptions3 = childSubscriptions3;
	}

	public List<Subscription> getChildSubscriptions4() {
		return childSubscriptions4;
	}

	public void setChildSubscriptions4(List<Subscription> childSubscriptions4) {
		this.childSubscriptions4 = childSubscriptions4;
	}

	public List<Subscription> getChildSubscriptions5() {
		return childSubscriptions5;
	}

	public void setChildSubscriptions5(List<Subscription> childSubscriptions5) {
		this.childSubscriptions5 = childSubscriptions5;
	}

	public List<Subscription> getChildSubscriptions6() {
		return childSubscriptions6;
	}

	public void setChildSubscriptions6(List<Subscription> childSubscriptions6) {
		this.childSubscriptions6 = childSubscriptions6;
	}

	public List<Integer> getSelectedSubscriptions1() {
		return selectedSubscriptions1;
	}

	public void setSelectedSubscriptions1(List<Integer> selectedSubscriptions1) {
		this.selectedSubscriptions1 = selectedSubscriptions1;
	}

	public List<Integer> getSelectedSubscriptions2() {
		return selectedSubscriptions2;
	}

	public void setSelectedSubscriptions2(List<Integer> selectedSubscriptions2) {
		this.selectedSubscriptions2 = selectedSubscriptions2;
	}

	public List<Integer> getSelectedSubscriptions3() {
		return selectedSubscriptions3;
	}

	public void setSelectedSubscriptions3(List<Integer> selectedSubscriptions3) {
		this.selectedSubscriptions3 = selectedSubscriptions3;
	}

	public List<Integer> getSelectedSubscriptions4() {
		return selectedSubscriptions4;
	}

	public void setSelectedSubscriptions4(List<Integer> selectedSubscriptions4) {
		this.selectedSubscriptions4 = selectedSubscriptions4;
	}

	public List<Integer> getSelectedSubscriptions5() {
		return selectedSubscriptions5;
	}

	public void setSelectedSubscriptions5(List<Integer> selectedSubscriptions5) {
		this.selectedSubscriptions5 = selectedSubscriptions5;
	}

	public List<Integer> getSelectedSubscriptions6() {
		return selectedSubscriptions6;
	}

	public void setSelectedSubscriptions6(List<Integer> selectedSubscriptions6) {
		this.selectedSubscriptions6 = selectedSubscriptions6;
	}

	public Project getProjectToModify() {
		return projectToModify;
	}

	public void setProjectToModify(Project projectToModify) {
		this.projectToModify = projectToModify;
	}

	public Project getProjectEdited() {
		return projectEdited;
	}

	public void setProjectEdited(Project projectEdited) {
		this.projectEdited = projectEdited;
	}

	public Project getProjectToCancel() {
		return projectToCancel;
	}

	public void setProjectToCancel(Project projectToCancel) {
		this.projectToCancel = projectToCancel;
	}

	public String getNewProjectCancelCause() {
		return newProjectCancelCause;
	}

	public void setNewProjectCancelCause(String newProjectCancelCause) {
		this.newProjectCancelCause = newProjectCancelCause;
	}

	public Project getProjectToClose() {
		return projectToClose;
	}

	public void setProjectToClose(Project projectToClose) {
		this.projectToClose = projectToClose;
	}

	public boolean isSelectedSuccess() {
		return selectedSuccess;
	}

	public void setSelectedSuccess(boolean selectedSuccess) {
		this.selectedSuccess = selectedSuccess;
	}

	public String getNewClosingComment() {
		return newClosingComment;
	}

	public void setNewClosingComment(String newClosingComment) {
		this.newClosingComment = newClosingComment;
	}

}