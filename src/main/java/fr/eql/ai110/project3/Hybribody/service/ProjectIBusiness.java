package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Customer;
import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;

public interface ProjectIBusiness {

	List<Project> getProjectsInProgress();

	public Project getByIdProject(Integer idProject);

	public Project getByExperiment(Experiment experiment);

	public List<Project> getAllInProgressAndMotherPlateInProgress();

	public List<Project> getAllInProgressWithRoundInProgress();

	public List<Project> getAll();

	public List<Project> search(String projectName, String customerSurname, String customerName, String xpCategory,
			Integer subscriptionName, String selectedStatus);
	
	public List<Project> getAllByAvailableScreeningWithAvailableRound();
	public boolean verifyAvailableScreeningWithAvailableRound();
	public boolean verifyAvailableScreeningWithAvailableRoundByIdProject(Integer idProject);
	public Long countAllProjectInTheLab();
	public Long countAllProjectInProgress();
	public Long countAllProjectClosingWithSuccess();
	public Long countAllProjectClosingWithFail();
	public Long countAllProjectCanceled();
	/**
	 * Allow to create a new project
	 * @param inputName
	 * @param inputComment
	 * @param subscriptionsChoice
	 * @param customerOrdering
	 * @param employee
	 * @return
	 */
	Project create(String inputName, String inputComment, List<Integer> subscriptionsChoice, Customer customerOrdering, Employee employee);
	/**
	 * Allow to modify a project
	 * @param projectToModify
	 * @param inputNameModify
	 * @param inputCommentModify
	 * @param subscriptionsChoiceModify
	 * @param customerOrderingModify
	 * @param employee
	 * @return
	 */
	Project modify(Project projectToModify, String inputNameModify, String inputCommentModify, List<Integer> subscriptionsChoiceModify, Customer customerOrderingModify, Employee employee);
	/**
	 * Allow to cancel a project
	 * @param projectToCancel
	 * @param inputCancelCause
	 * @param employee
	 * @return
	 */
	Project cancel(Project projectToCancel, String inputCancelCause, Employee employee);
	Project close(Project projectToClose, String newClosingComment, boolean selectedSuccess, Employee employee);
	Project reopen(Project projectToReOpen, Employee employee);
	List<Project> getSimilarProjects(Project project);
}
