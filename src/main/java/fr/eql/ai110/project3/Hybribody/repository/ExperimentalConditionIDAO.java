package fr.eql.ai110.project3.Hybribody.repository;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Buffer;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalCondition;
import fr.eql.ai110.project3.Hybribody.model.ScientificValue;

public interface ExperimentalConditionIDAO extends CrudRepository<ExperimentalCondition, Long> {

	ExperimentalCondition findByTemperatureAndBufferAndApplication(ScientificValue temperature, Buffer buffer, String application);
}
