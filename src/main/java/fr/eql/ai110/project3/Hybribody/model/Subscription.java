package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

@Entity
@Table(name = "subscription")
public class Subscription implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idsubscription")
	private Integer idsubscription;
	@Column(name = "idsubscriptionparent")
	private Integer idSubscriptionParent;
	@Column(name = "name")
	private String name;
	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable( name = "T_subscription_project_Associations",
                joinColumns = @JoinColumn(name="idsubscription"),
                inverseJoinColumns = @JoinColumn( name = "idproject" ) )
	private List<Project> projects;

	public Subscription(Integer idsubscription, Integer idSubscriptionParent, String name, List<Project> projects) {
		super();
		this.idsubscription = idsubscription;
		this.idSubscriptionParent = idSubscriptionParent;
		this.name = name;
		this.projects = projects;
	}

	public Subscription() {
		super();
	}

	public Integer getIdsubscription() {
		return idsubscription;
	}

	public void setIdsubscription(Integer idsubscription) {
		this.idsubscription = idsubscription;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Integer getIdSubscriptionParent() {
		return idSubscriptionParent;
	}

	public void setIdSubscriptionParent(Integer idSubscriptionParent) {
		this.idSubscriptionParent = idSubscriptionParent;
	}
	
}
