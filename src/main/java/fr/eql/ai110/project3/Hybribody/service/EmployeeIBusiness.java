package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Employee;

public interface EmployeeIBusiness {

	/**
	 * Get all the employees in the data base and order by date arriving in the laboratory
	 * @return a list of employees
	 */
	public List<Employee> getAllOrderByDateArrivingAsc();
	/**
	 * Get a employee by the mail (the mail is unique)
	 * @param mail : input by the user
	 * @return a employee who matched with the mail.
	 */
	public Employee getByLogin(String mail);
}
