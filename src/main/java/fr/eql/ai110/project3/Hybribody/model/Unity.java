package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "unity")
public class Unity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idunity")
	private Integer id;
	@Column(name="name")
	private String name;
	@ManyToMany
	@JoinTable(name = "T_unity_valuecategory_Associations",
	joinColumns = @JoinColumn(name = "idunity"),
	inverseJoinColumns = @JoinColumn(name = "idvaluecategory"))
	List<ValueCategory> categories;
	@OneToMany(mappedBy = "unity", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	List<ScientificValue> values;
	
	public Unity() {
		super();
	}

	public Unity(Integer id, String name, List<ValueCategory> categories, List<ScientificValue> values) {
		super();
		this.id = id;
		this.name = name;
		this.categories = categories;
		this.values = values;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ValueCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<ValueCategory> categories) {
		this.categories = categories;
	}

	public List<ScientificValue> getValues() {
		return values;
	}

	public void setValues(List<ScientificValue> values) {
		this.values = values;
	}
	
}
