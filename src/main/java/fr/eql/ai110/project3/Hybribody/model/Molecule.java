package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="molecule")
public class Molecule implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_molecule")
	private Integer id;
	@Column(name="name")
	private String name;
	@ManyToOne
	@JoinColumn(name = "idvalue")
	private ScientificValue concentration;
	@ManyToMany
	@JoinTable(name = "T_buffer_molecule_Associations",
			joinColumns = @JoinColumn(name = "id_molecule"),
			inverseJoinColumns = @JoinColumn(name = "idconsumable"))
	private List<Buffer> buffers;

	public Molecule() {
		super();
	}

	public Molecule(Integer id, String name, ScientificValue concentration, List<Buffer> buffers) {
		super();
		this.id = id;
		this.name = name;
		this.concentration = concentration;
		this.buffers = buffers;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ScientificValue getConcentration() {
		return concentration;
	}

	public void setConcentration(ScientificValue concentration) {
		this.concentration = concentration;
	}

	public List<Buffer> getBuffers() {
		return buffers;
	}

	public void setBuffers(List<Buffer> buffers) {
		this.buffers = buffers;
	}

}
