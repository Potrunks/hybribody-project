package fr.eql.ai110.project3.Hybribody.repository;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.project3.Hybribody.model.Target;

public interface TargetIDAO extends CrudRepository<Target, Long> {

	
}
