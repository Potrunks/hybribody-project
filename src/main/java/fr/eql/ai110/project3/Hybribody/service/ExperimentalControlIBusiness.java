package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;

public interface ExperimentalControlIBusiness {

	/**
	 * Get all the controls used on the mother plate picking
	 * @param motherPlatePicking : mother plate picking who want to obtain the controls
	 * @return a list of experimental control of the mother plate picking
	 */
	List<ExperimentalControl> getControls(MotherPlatePicking motherPlatePicking);
	/**
	 * Get all the experimental controls of the data base without one control of your choice
	 * @param controlDidNotWant : the control who don't want
	 * @return a list of controls without the control who don't want
	 */
	List<ExperimentalControl> getControlsWithoutOneControlOfChoice(String controlDidNotWant);
	/**
	 * Get the name of the experimental control by her ID
	 * @param idXpControl : ID of the control wanted
	 * @return the name of the control wanted
	 */
	String getNameByID(Integer idXpControl);
	/**
	 * Get the position in a 96 well plate of the control wanted
	 * @param idXpControl : ID of the control wanted
	 * @return the position of the control (string) wanted 
	 */
	String getPositionByID(Integer idXpControl);
	/**
	 * Get all the controls by a position in a 96 well plate
	 * @param position : position wanted in a 96 well plate
	 * @return the list of a controls by the position wanted
	 */
	List<ExperimentalControl> getControlsByPosition(String position);
	/**
	 * Get all the controls by different ID
	 * @param idXPControls : list of ID experimental controls wanted
	 * @return all the experimental controls by a list of ID
	 */
	List<ExperimentalControl> getByMultipleIdXPControls(List<Integer> idXPControls);
	/**
	 * Get a experimental control by a name
	 * @param name : string of the name wanted
	 * @return a experimental control matched with the name
	 */
	ExperimentalControl getByName(String name);
}
