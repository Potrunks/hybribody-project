package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "label")
public class Label implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idlabel")
	private Integer id;
	@Column(name="name")
	private String name;
	@OneToMany(mappedBy = "label", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Tag> tags;
	@OneToMany(mappedBy = "label", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Target> targets;
	
	public Label() {
		super();
	}

	public Label(Integer id, String name, List<Tag> tags, List<Target> targets) {
		super();
		this.id = id;
		this.name = name;
		this.tags = tags;
		this.targets = targets;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}
	
}
