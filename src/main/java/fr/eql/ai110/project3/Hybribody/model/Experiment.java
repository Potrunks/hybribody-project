package fr.eql.ai110.project3.Hybribody.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="category")
@Table(name = "experiment")
public class Experiment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idexperiment")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "datecreation")
	private LocalDate dateCreation;
	@Column(name = "datevalidation")
	private LocalDate dateValidation;
	@Column(name = "datecancelation")
	private LocalDate dateCancelation;
	@Column(name = "valid")
	private boolean valid;
	@Column(name = "validationcomment")
	private String validationComment;
	@Column(name = "cancelationcomment")
	private String cancelationComment;
	@Column(name = "comment")
	private String comment;
	@ManyToOne
	@JoinColumn(name = "idemployee")
	private Employee employee;
	@ManyToMany
	@JoinTable(name = "T_experiment_project_Associations",
			joinColumns = @JoinColumn(name = "idexperiment"),
			inverseJoinColumns = @JoinColumn(name = "idproject"))
	private List<Project> projects;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "T_experiment_xpcondition_Associations",
			joinColumns = @JoinColumn(name = "idexperiment"),
			inverseJoinColumns = @JoinColumn(name = "id_xp_condition"))
	private List<ExperimentalCondition> xpConditions;
	
	public Experiment() {
		super();
	}

	public Experiment(Integer id, String name, LocalDate dateCreation, LocalDate dateValidation,
			LocalDate dateCancelation, boolean valid, String validationComment, String cancelationComment,
			String comment, Employee employee, List<Project> projects, List<ExperimentalCondition> xpConditions) {
		super();
		this.id = id;
		this.name = name;
		this.dateCreation = dateCreation;
		this.dateValidation = dateValidation;
		this.dateCancelation = dateCancelation;
		this.valid = valid;
		this.validationComment = validationComment;
		this.cancelationComment = cancelationComment;
		this.comment = comment;
		this.employee = employee;
		this.projects = projects;
		this.xpConditions = xpConditions;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public LocalDate getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(LocalDate dateValidation) {
		this.dateValidation = dateValidation;
	}
	

	public boolean isValid() {
		return valid;
	}


	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getValidationComment() {
		return validationComment;
	}

	public void setValidationComment(String validationComment) {
		this.validationComment = validationComment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public List<ExperimentalCondition> getXpConditions() {
		return xpConditions;
	}

	public void setXpConditions(List<ExperimentalCondition> xpConditions) {
		this.xpConditions = xpConditions;
	}

	public LocalDate getDateCancelation() {
		return dateCancelation;
	}

	public void setDateCancelation(LocalDate dateCancelation) {
		this.dateCancelation = dateCancelation;
	}

	public String getCancelationComment() {
		return cancelationComment;
	}

	public void setCancelationComment(String cancelationComment) {
		this.cancelationComment = cancelationComment;
	}
	
}
