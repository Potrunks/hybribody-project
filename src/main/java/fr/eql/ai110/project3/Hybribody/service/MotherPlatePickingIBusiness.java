package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalCondition;
import fr.eql.ai110.project3.Hybribody.model.ExperimentalControl;
import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;

public interface MotherPlatePickingIBusiness {

	MotherPlatePicking getById (Integer idExperiment);
	List<MotherPlatePicking> getAllInProgressByProject(Project project);
	List<MotherPlatePicking> getAllInProgressByIdProject(Integer idProject);
	/**
	 * validate() allow to validate the mother picking plate and if the experiment
	 * is a success this method create new antibodies in the data base being careful
	 * about the position of the experimental control
	 * 
	 * @param idExperiment : ID of the Mother Plate Picking
	 * @param isSuccess : boolean to indicate the success or not of the Mother Plate Picking
	 * @param failureCause : string input of the user to add a comment about the result of the Mother Plate Picking
	 */
	void validate(Integer idExperiment, boolean isSuccess, String failureCause);
	boolean verifyMotherPlateInProgressExist();
	/**
	 * Allow to create a new Mother Plate Picking in order to add it in the data base.
	 * In addition, a task is created with the name of the project and the Mother Plate Picking
	 * associated to an employee
	 * @param pickedRounds : Screening round used for the Mother Plate Picking
	 * @param controls : Experimental controls used for the Mother Plate Picking
	 * @param employee : Employee who create the new Mother Plate Picking
	 * @param projects : Projects attached to the new Mother Plate Picking
	 * @param xpConditions : Experimental Conditions used for the new Mother Plate Picking
	 */
	void create(List<ScreeningRound> pickedRounds, List<ExperimentalControl> controls,
			Employee employee, List<Project> projects, List<ExperimentalCondition> xpConditions);
}