package fr.eql.ai110.project3.Hybribody.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.project3.Hybribody.model.Customer;
import fr.eql.ai110.project3.Hybribody.model.Employee;
import fr.eql.ai110.project3.Hybribody.model.Experiment;
import fr.eql.ai110.project3.Hybribody.model.Project;
import fr.eql.ai110.project3.Hybribody.model.Subscription;
import fr.eql.ai110.project3.Hybribody.model.Task;
import fr.eql.ai110.project3.Hybribody.repository.ProjectIDAO;
import fr.eql.ai110.project3.Hybribody.repository.SubscriptionIDAO;
import fr.eql.ai110.project3.Hybribody.repository.TaskIDAO;
import fr.eql.ai110.project3.Hybribody.service.ProjectIBusiness;

@Service
public class ProjectBusiness implements ProjectIBusiness {

	@Autowired
	private ProjectIDAO projectDAO;
	@Autowired
	private TaskIDAO taskDAO;
	@Autowired
	private SubscriptionIDAO subscriptionDAO;

	@Override
	public List<Project> getProjectsInProgress() {
		return projectDAO.findByDateClosingIsNullAndDateCancelationIsNullOrderByDateCreationDesc();
	}

	@Override
	public Project getByIdProject(Integer idProject) {
		return projectDAO.findByIdproject(idProject);
	}

	@Override
	public Project getByExperiment(Experiment experiment) {
		return projectDAO.findByExperiments(experiment);
	}

	@Override
	public List<Project> getAllInProgressAndMotherPlateInProgress() {
		return projectDAO.findAllInProgressAndMotherPlateInProgress();
	}

	@Override
	public List<Project> getAllInProgressWithRoundInProgress() {
		return projectDAO.findAllInProgressAndScreeningRoundInProgress();
	}

	@Override
	public List<Project> getAll() {
		return (List<Project>) projectDAO.findAll();
	}

	@Override
	public List<Project> search(String projectName, String customerSurname, String customerName, String xpCategory,
			Integer subscriptionName, String selectedStatus) {
		List<Project> searchResult = new ArrayList<Project>();
		switch (selectedStatus) {
		case "All":
			searchResult = getAllByMultiCriteria(formatStringForQueryWithLike(projectName),
					formatStringForQueryWithLike(customerSurname), formatStringForQueryWithLike(customerName),
					formatStringForQueryWithLike(xpCategory), subscriptionName);
			break;
		case "In Progress":
			searchResult = getInProgressByMultiCriteria(formatStringForQueryWithLike(projectName),
					formatStringForQueryWithLike(customerSurname), formatStringForQueryWithLike(customerName),
					formatStringForQueryWithLike(xpCategory), subscriptionName);
			break;
		case "Successfull Validation":
			searchResult = getSuccessfullyValidateByMultiCriteria(formatStringForQueryWithLike(projectName),
					formatStringForQueryWithLike(customerSurname), formatStringForQueryWithLike(customerName),
					formatStringForQueryWithLike(xpCategory), subscriptionName);
			break;
		case "Unsuccessfull Validation":
			searchResult = getUnsuccessfullyValidateByMultiCriteria(formatStringForQueryWithLike(projectName),
					formatStringForQueryWithLike(customerSurname), formatStringForQueryWithLike(customerName),
					formatStringForQueryWithLike(xpCategory), subscriptionName);
			break;
		case "Cancel":
			searchResult = getCancelByMultiCriteria(formatStringForQueryWithLike(projectName),
					formatStringForQueryWithLike(customerSurname), formatStringForQueryWithLike(customerName),
					formatStringForQueryWithLike(xpCategory), subscriptionName);
			break;
		default:
			searchResult = getAll();
			break;
		}
		return searchResult;
	}

	private List<Project> getInProgressByMultiCriteria(String projectName, String customerSurname, String customerName,
			String xpCategory, Integer subscriptionName) {
		return projectDAO.findInProgressByMultiCriteria(projectName, customerSurname, customerName, xpCategory,
				subscriptionName);
	}

	private List<Project> getAllByMultiCriteria(String projectName, String customerSurname, String customerName,
			String xpCategory, Integer subscriptionName) {
		return projectDAO.findAllByMultiCriteria(projectName, customerSurname, customerName, xpCategory,
				subscriptionName);
	}

	private List<Project> getSuccessfullyValidateByMultiCriteria(String projectName, String customerSurname,
			String customerName, String xpCategory, Integer subscriptionName) {
		return projectDAO.findSuccessfullyValidateByMultiCriteria(projectName, customerSurname, customerName,
				xpCategory, subscriptionName);
	}

	private List<Project> getUnsuccessfullyValidateByMultiCriteria(String projectName, String customerSurname,
			String customerName, String xpCategory, Integer subscriptionName) {
		return projectDAO.findUnsuccessfullyValidateByMultiCriteria(projectName, customerSurname, customerName,
				xpCategory, subscriptionName);
	}

	private List<Project> getCancelByMultiCriteria(String projectName, String customerSurname, String customerName,
			String xpCategory, Integer subscriptionName) {
		return projectDAO.findCancelByMultiCriteria(projectName, customerSurname, customerName, xpCategory,
				subscriptionName);
	}

	public String formatStringForQueryWithLike(String stringNoFormat) {
		String stringFormat = "%" + formatStringForRemovingSpecialChar(stringNoFormat) + "%";
		System.out.println(stringFormat);
		return stringFormat;
	}

	public String formatStringForRemovingSpecialChar(String stringNoFormat) {
		Pattern p = Pattern.compile("\\d|\\W");
		Matcher m = p.matcher(stringNoFormat);
		String stringFormat = m.replaceAll("");
		return stringFormat;
	}

	@Override
	public List<Project> getAllByAvailableScreeningWithAvailableRound() {
		return projectDAO.findByAvailableScreeningAndAvailableRound();
	}

	@Override
	public boolean verifyAvailableScreeningWithAvailableRound() {
		boolean exist = false;
		if (projectDAO.countByAvailableScreeningAndAvailableRound() != 0) {
			exist = true;
		}
		return exist;
	}

	@Override
	public boolean verifyAvailableScreeningWithAvailableRoundByIdProject(Integer idProject) {
		boolean exist = false;
		if (projectDAO.countByAvailableScreeningAndAvailableRoundAndIdProject(idProject) != 0) {
			exist = true;
		}
		return exist;
	}

	@Override
	public Long countAllProjectInTheLab() {
		return projectDAO.countByDateCreationIsNotNull();
	}

	@Override
	public Long countAllProjectClosingWithSuccess() {
		return projectDAO.countByDateCreationIsNotNullAndDateClosingIsNotNullAndDateCancelationIsNullAndSuccessTrue();
	}

	@Override
	public Long countAllProjectClosingWithFail() {
		return projectDAO.countByDateCreationIsNotNullAndDateClosingIsNotNullAndDateCancelationIsNullAndSuccessFalse();
	}

	@Override
	public Long countAllProjectCanceled() {
		return projectDAO.countByDateCreationIsNotNullAndDateClosingIsNullAndDateCancelationIsNotNull();
	}

	@Override
	public Long countAllProjectInProgress() {
		return projectDAO.countByDateCreationIsNotNullAndDateClosingIsNullAndDateCancelationIsNull();
	}

	@Override
	public Project create(String inputName, String inputComment, List<Integer> subscriptionsChoice,
			Customer customerOrdering, Employee employee) {
		if (inputComment.equals("")) {
			inputComment = "No Comment";
		}
		Project newProject = new Project(null, inputName, LocalDate.now(), null, null, inputComment, false, null, null,
				null, null, customerOrdering);
		newProject = projectDAO.save(newProject);
		newProject.setSubscriptions(subscriptionsGenerator(subscriptionsChoice));
		projectDAO.save(newProject);
		createTaskForNewProject(newProject, employee);
		return newProject;
	}

	private List<Subscription> subscriptionsGenerator(List<Integer> subscriptionsChoice) {
		List<Subscription> subscriptions = new ArrayList<Subscription>();
		for (Integer idSelectedSubscription : subscriptionsChoice) {
			subscriptions.add(subscriptionDAO.findByIdsubscription(idSelectedSubscription));
		}
		return subscriptions;
	}

	private void createTaskForNewProject(Project newProject, Employee employee) {
		Task newTask = new Task(null, "Project " + newProject.getName() + " created", LocalDateTime.now(), employee,
				null);
		newTask.setDateFormat(newTask.getDateCreation());
		taskDAO.save(newTask);
	}

	@Override
	public Project modify(Project projectToModify, String inputNameModify, String inputCommentModify,
			List<Integer> subscriptionsChoiceModify, Customer customerOrderingModify, Employee employee) {
		if (inputCommentModify.equals("")) {
			inputCommentModify = "No Comment";
		}
		projectToModify.setName(inputNameModify);
		projectToModify.setComment(inputCommentModify);
		projectToModify.setSubscriptions(subscriptionsGenerator(subscriptionsChoiceModify));
		projectToModify.setCustomer(customerOrderingModify);
		createTaskForModifyProject(projectDAO.save(projectToModify), employee);
		return projectToModify;
	}

	private void createTaskForModifyProject(Project modifyProject, Employee employee) {
		Task newTask = new Task(null, "Project " + modifyProject.getName() + " modified", LocalDateTime.now(), employee,
				null);
		newTask.setDateFormat(newTask.getDateCreation());
		taskDAO.save(newTask);
	}

	@Override
	public Project cancel(Project projectToCancel, String inputCancelCause, Employee employee) {
		projectToCancel.setCancelationCause(inputCancelCause);
		projectToCancel.setDateCancelation(LocalDate.now());
		projectToCancel.setDateClosing(null);
		projectToCancel = projectDAO.save(projectToCancel);
		createTaskForCancelProject(projectToCancel, employee);
		return projectToCancel;
	}

	private void createTaskForCancelProject(Project projectToCancel, Employee employee) {
		Task newTask = new Task(null, "Project " + projectToCancel.getName() + " canceled", LocalDateTime.now(),
				employee, null);
		newTask.setDateFormat(newTask.getDateCreation());
		taskDAO.save(newTask);
	}

	@Override
	public Project close(Project projectToClose, String newClosingComment, boolean selectedSuccess, Employee employee) {
		projectToClose.setDateClosing(LocalDate.now());
		if (newClosingComment.equals("")) {
			newClosingComment = "No Comment";
		}
		projectToClose.setClosingComment(newClosingComment);
		projectToClose.setSuccess(selectedSuccess);
		projectToClose = projectDAO.save(projectToClose);
		createTaskForCloseProject(projectToClose, employee);
		return projectToClose;
	}

	private void createTaskForCloseProject(Project projectToClose, Employee employee) {
		Task newTask = new Task(null, "Project " + projectToClose.getName() + " closed", LocalDateTime.now(), employee,
				null);
		newTask.setDateFormat(newTask.getDateCreation());
		taskDAO.save(newTask);
	}

	@Override
	public Project reopen(Project projectToReOpen, Employee employee) {
		projectToReOpen.setClosingComment(null);
		projectToReOpen.setDateClosing(null);
		projectToReOpen.setSuccess(false);
		createTaskForReopenProject(projectToReOpen, employee);
		return projectDAO.save(projectToReOpen);
	}

	private void createTaskForReopenProject(Project projectToReOpen, Employee employee) {
		Task newTask = new Task(null, "Project " + projectToReOpen.getName() + " re-opened", LocalDateTime.now(),
				employee, null);
		newTask.setDateFormat(newTask.getDateCreation());
		taskDAO.save(newTask);
	}

	@Override
	public List<Project> getSimilarProjects(Project project) {
		List<Project> similarProjects = new ArrayList<Project>();
		String[] projectNameSplit = project.getName().split("\\W");
		String currentString = "%" + projectNameSplit[0] + "%";
		similarProjects.addAll(projectDAO.findFirst3ByNameLikeAndIdprojectNotOrderByDateCreationDesc(currentString, project.getIdproject()));
		return similarProjects;
	}
}