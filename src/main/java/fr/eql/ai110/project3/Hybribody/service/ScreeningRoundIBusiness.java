package fr.eql.ai110.project3.Hybribody.service;

import java.util.List;

import fr.eql.ai110.project3.Hybribody.model.MotherPlatePicking;
import fr.eql.ai110.project3.Hybribody.model.Screening;
import fr.eql.ai110.project3.Hybribody.model.ScreeningRound;

public interface ScreeningRoundIBusiness {

	public List<ScreeningRound> getRoundsValidatedAndSuccess(Screening screening);
	public List<ScreeningRound> getRoundsValidated(Screening screening);
	public List<ScreeningRound> getRoundsInProgress(Screening screening);
	public List<ScreeningRound> getRoundsPicked(MotherPlatePicking motherPlatePicking);
	public ScreeningRound getById(Integer idScreeningRound);
	boolean verifyByScreeningIfPreviousRoundExist(Integer idScreening, Integer idRound);
	/**
	 * Validate a screening round by updating this screening round with a comment and a boolean success 
	 * @param idScreeningRound
	 * @param validationCause
	 * @param isSuccess
	 */
	public void validate(Integer idScreeningRound, String validationCause, boolean isSuccess);
	boolean verifyRoundInProgressExist();
	public List<ScreeningRound> getByMultipleIdScreeningRound(List<Integer> idRounds);
}
