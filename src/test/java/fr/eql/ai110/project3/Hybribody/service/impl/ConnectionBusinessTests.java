package fr.eql.ai110.project3.Hybribody.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ConnectionBusinessTests {

	@Test
	public void stringNotHashed_returnStringHashed() {
		String stringToTest = "Trunks92!8js";
		ConnectionBusiness connectionBusiness = new ConnectionBusiness();
		assertEquals("�G�g7���4�H[����5����,����38", connectionBusiness.hashedPassword(stringToTest));
	}
}
