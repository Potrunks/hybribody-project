package fr.eql.ai110.project3.Hybribody.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import fr.eql.ai110.project3.Hybribody.model.Experiment;

public class ExperimentBusinessTests {

		@Test
		public void noMostExperimentInProgress_ReturnNoXPInProgress() {
			ExperimentBusiness xpBusiness = new ExperimentBusiness();
			Experiment xp = new Experiment();
			xp = null;
			assertEquals("No Experiment In Progress", xpBusiness.verifyIfTheMostXPIsPresent(xp));
		}
		
		@Test
		public void mostXPInProgress_NoReturnNoXPInProgress() {
			ExperimentBusiness xpBusiness = new ExperimentBusiness();
			Experiment xp = new Experiment();
			xp.setName("Is Alive!!!");
			assertNotEquals("No Experiment In Progress", xpBusiness.verifyIfTheMostXPIsPresent(xp));
		}
		
		@Test
		public void noMostXPValidated_ReturnNoXpValidated() {
			ExperimentBusiness xpBusiness = new ExperimentBusiness();
			Experiment xp = new Experiment();
			xp = null;
			assertEquals("No Experiment Validated", xpBusiness.verifyIfTheMostValidatedIsPresent(xp));
		}
		
		@Test
		public void mostXPValidated_NoReturnNoXPValidated() {
			ExperimentBusiness xpBusiness = new ExperimentBusiness();
			Experiment xp = new Experiment();
			xp.setName("Is Alive!!!");
			assertNotEquals("No Experiment Validated", xpBusiness.verifyIfTheMostValidatedIsPresent(xp));
		}
		
		@Test
		public void xpInProgressExist_returnTrue() {
			Experiment xp = new Experiment(1, "Initilization Mutation", null, null, null, true, "More Power", "Need Power", null, null, null, null);
			ExperimentBusiness xpBusiness = new ExperimentBusiness();
			assertTrue(xpBusiness.xpInProgressExistVerificator(xp));
		}
		
		@Test
		public void xpInProgressDontExist_returnFalse() {
			Experiment xp = null;
			ExperimentBusiness xpBusiness = new ExperimentBusiness();
			assertFalse(xpBusiness.xpInProgressExistVerificator(xp));
		}
}
