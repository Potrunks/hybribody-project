package fr.eql.ai110.project3.Hybribody.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

public class ProjectBusinessTests {

	@Test
	public void stringFormatForQueryWithLike_ReturnPercentageSymbolAsFirstAndLastChar() {
		ProjectBusiness projectBusiness = new ProjectBusiness();
		String stringTest = projectBusiness.formatStringForQueryWithLike("Test");
		assertEquals("%Test%", stringTest);
	}
	
	@Test
	public void stringWithNoSpecialChar_IsCorrect() {
		ProjectBusiness projectBusiness = new ProjectBusiness();
		String stringTest = projectBusiness.formatStringForRemovingSpecialChar("Test");
		Pattern p = Pattern.compile("\\d|\\W");
		Matcher m = p.matcher(stringTest);
		assertFalse(m.matches());
	}
	
	@Test
	public void stringWithSpecialChar_AreRemoved() {
		ProjectBusiness projectBusiness = new ProjectBusiness();
		String stringTest = projectBusiness.formatStringForRemovingSpecialChar("9&Te'65s't");
		assertEquals("Test", stringTest);
	}
}
